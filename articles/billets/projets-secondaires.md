---
title: Projets secondaires
date: 2022-03-15 10:42
---

J’accumule les projets secondaires depuis des années, principalement des bouts de code. J’ai expliqué il y a quelques semaines avoir commencé à « [terminer](terminer-ses-projets.html) » ces projets, mais je n’avais pas de recul sur ce qu’il me restait à faire. Je me suis donc créé un document synthétique :

- 13 sont déjà terminés / archivés ;
- 10 pourraient l’être assez rapidement ;
- 3 de plus seront amenés à l’être à moyen terme.

Il me resterait, en gros, 3 projets à maintenir (dont [GDPR.txt](https://gdpr-txt.org)), auxquels s’ajoutent la maintenance de différents sites ainsi que… tout ce que je fais dans le cadre de [Flus](https://flus.fr) et [Framasoft](https://framasoft.org), voire [FreshRSS](https://freshrss.org) dans une moindre mesure. Bref, ça représente encore un peu de taf.

J’ai (quasiment) tout déplacé de GitHub vers Framagit, vous trouverez donc (quasiment) tous ces projets [sur ce dernier](https://framagit.org/users/marienfressinaud/projects). Une fois que j’aurai fini d’archiver ce qui peut l’être, je pense que je créerai une page dédiée sur ce site : certains projets mériteraient mieux qu’un dépôt de code perdu dans un coin.