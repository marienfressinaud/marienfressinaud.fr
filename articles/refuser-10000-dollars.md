---
title: Refuser 10 000 dollars
date: 2023-05-03 18:30
---

Aujourd’hui, j’ai refusé 10 000 dollars.

J’ai reçu un mail en provenance d’un bureau d’enregistrement de noms de domaine — transmis par OVH — me proposant de me racheter un nom de domaine.
Voici le message (après traduction) :

> Je vous contacte au nom d’un de mes clients qui est très intéressé pour vous acheter votre nom de domaine lessy.io et je voulais savoir si vous étiez prêt à le vendre. Nous aimerions vous faire une offre de 10K USD.

Pour ce que j’en ai vérifié, la demande me semble légitime et honnête.
Quand bien même il s’agirait d’hameçonnage, ça ne change pas la principale raison de mon refus : **je n’ai pas envie d’aider une boîte ou une personne qui a les moyens de mettre 10 000 dollars dans un nom de domaine.
On ne fait pas partie du même monde.**

Alors non, ce choix n’est pas rationnel.
En revanche, il est politique.
