---
title: Concevoir les logiciels de demain
date: 2019-04-16 11:05
slug: talks/concevoir-les-logiciels-de-demain
---

J’ai donné le dimanche 7 avril dernier une conférence à l’occasion des
[Journées du Logiciel Libre](https://jdll.org) (<abbr>JdLL</abbr>). L’article
qui suit correspond à la transcription de ce que j’y ai raconté. Je me
proposais de poser des réflexions autour de la conception des logiciels de
« demain », en intégrant à la fois des considérations environementales et
d’impact utilisateurice. Loin d’être exhaustive, cette présentation était
surtout l’occasion de jeter quelques pistes à défricher ensuite ensemble. Le
format lui-même tentait de faire intervenir les personnes venues m’écouter. Sur
ce point j’ai été particulièrement satisfait que les conversations se déroulent
de manière aussi fluide, sans accaparement de la parole ; vraiment merci aux
personnes qui étaient là ! J’ignore si elles sont reparties avec ce qu’elles
étaient venues chercher, c’était finalement assez éloigné de ce que j’avais en
tête au moment de proposer le sujet. J’ai en tout cas pris plaisir à donner
cette conférence (même pas stressé !) Pour bien faire, il faudrait maintenant
que je creuse chaque sujet dans des articles spécifiques, il y a de quoi faire.

La transcription est plus complète que ce que j’ai raconté à l’oral vu que j’ai
nécessairement oublié de dire des choses. Je l’ai aussi annotée pour redonner
du contexte lorsque nécessaire. Les quelques diapos [sont accessibles en suivant
ce lien](concevoir-les-logiciels-de-demain/diapos.html).

---

## Introduction

Je vais commencer par vous poser une question : qui ici s’est déjà posé la
question d’adopter un mode de vie numérique plus en accord avec la planète ?

> Note : tout le monde ou presque a levé la main alors que la salle était
> pleine. Très content de cette réponse même si du coup ça a augmenté mon
> syndrome de l’imposteur. Ça a toutefois bien aidé à alimenter les discussions
> qui ont suivi.

Si je vous pose la question, c’est parce qu’en préparant cette conférence, je
me suis rendu compte que je n’arriverais jamais à aborder tous les sujets qui
pourraient l’être. Je risque de perdre des gens, et je risque surtout de ne pas
répondre aux attentes de celleux qui se sont posées plus de questions que moi.
Alors j’aimerais faire de cette conférence un espace participatif en tentant un
format un peu particulier. Je vais introduire les deux parties de notre
conférence pendant 10 à 15 minutes chacune, et à la fin de chaque partie, je
vous poserai une question pour ouvrir le sujet. Le but étant de venir compléter
ce que j’aurais dit, ou de débattre selon les envies. Ce que je vais raconter
paraîtra sûrement incomplet aux yeux de certaines personnes, mon but n’est pas
forcément de rentrer dans le détail mais de défricher un certain nombre de
sujets… en les survolant donc.

Mon objectif est que vous repartiez avec trois éléments. Le premier, c’est que
vous vous posiez la question de savoir si votre vie numérique est en accord
avec vos convictions. Le deuxième, ce sont des idées, des outils pour concevoir
plus simple, plus orienté « bien-être » utilisateurices. Enfin, je voudrais
tracer quelques sillons de sujets que vous auriez envie de creuser,
d’approfondir en sortant de cette salle.

Je vous mets un pad à disposition pour retrouver la transcription de ce que je
vais raconter ainsi que les diapos. Mais je vous encourage, si vous prenez des
notes, à le faire de manière collaborative sur ce pad. Si vous avez des
questions, des retours à faire sur le fond comme sur la forme, allez-y. Si vous
voulez partager des liens, des ressources ou si vous voulez échanger des
coordonnées pour discuter de tout ça entre vous plus tard, faites-vous
plaisir !

> Note : personne n’avait son <abbr>PC</abbr> sur les genoux du coup l’idée
> du pad collaboratif est tombée à l’eau. Pas très grave, les gens étaient
> d’autant plus à l’écoute.

## Moi, super-héros et développeur de sites statiques

Pour commencer, je vais vous parler de moi. Et donc aussi un peu de vous, je
l’espère.

L’autre jour en trainant sur Twitter, je suis tombé sur une photo montrant un
ours sur la banquise, rachétique et visiblement mal en point. Le commentaire
sous la photo laissait entendre que son état était dû au réchauffement de la
planète. Du coup, ni une ni deux, un sentiment de révolte m’envahit ! « Comment
peut-on laisser faire ça ? » Mais l’ours n’est pas la seule raison de cette
colère : chaque tweet, chaque article traitant d’inégalité, de racisme ou
d’effondrement vient alimenter ce sentiment d’injustice, et par la même
occasion, cette envie de vouloir tout résoudre. Ce syndrome, c’est celui du
super-héros, celui qui est capable de tout remettre dans l’ordre. Nous avons
tous envie d’être un super-héros pour résoudre les problèmes dans le monde.
Mais notre kryptonite à nous, ce n’est pas un caillou radioactif, c’est les
photos de chats « vraiment-trop-mignons ». Invariablement après une telle
photo, notre colère s’estompe.

Heureusement, il y a celleux qui font. Celleux qui font pipi sous la douche
pour économiser l’eau de la planète. Celleux qui éteignent la télé avant de se
coucher pour sauver la banquise de ce pauvre ours famélique. Et il y a celleux
qui développent des sites statiques pour réduire leur impact carbone. Alors
oui, ça vous fait peut-être rire : « C’est pas comme ça qu’ils vont sauver le
monde, hé ! » Mais en attendant, est-ce qu’ils et elles ne font déjà pas un peu
plus que vous, en colère derrière votre écran ?

Évidemment, je vous présente des situations caricaturales, on est toutes et
tous un peu des deux à la fois, et il est hors de question de confronter
celleux qui font à celleux qui se révolent. Nous ne sommes pas toutes et tous
égaux face à l’action politique (au sens large du terme), notamment à cause de
nos inégalités face au temps, aux ressources ou à notre position sociale. Je
propose que nous restions tous humble vis-à-vis de ce que l’on peut
entreprendre et d’accepter que nous avons tous une part d’incohérence que nous
travaillons chacun·e à notre rythme. Je suis convaincu par ailleurs (c’est donc
subjectif) que ce sont nos actions individuelles (bien que parfois un peu
ridicules) qui nous font avancer sur le chemin d’une action plus collective et
plus efficace. C’est parce que j’ai rejoint une <abbr>AMAP</abbr> un jour que
j’ai eu l’idée de faire une conférence participative et que demain je
m’associerai peut-être à un collectif pour faire avancer les causes qui
m’importent, en espérant peut-être un effet boule de neige.

Mais revenons-en à notre sujet : j’ai moi-même développé un générateur de sites
statiques avec dans l’idée de faire « mieux ». Évidemment, je sens bien que
c’est totalement insuffisant, mon site personnel notamment a une toute petite
visibilité, quel impact pourrait-il bien avoir sur la planète ? Alors si mon
site statique est insuffisant, qu’est-ce que je fais ? J’ai essayé de
formaliser un peu les problèmes que le numérique me pose.

Partons de ce schéma représentant la répartition de notre empreinte carbone. On
y voit notamment que notre usage du numérique possède un impact aussi important
que celui de notre consommation de viande et de poisson et on peut parier sur
une augmentation au moins à court terme.

![schéma montrant que l’empreinte carbone des Français concernant nos achats et usage du numérique est de 1180 Kg eq CO2/an tandis que notre consommation de viande et de poisson est de
1144.](concevoir-les-logiciels-de-demain/empreinte-carbone.jpg)

Cet empreinte est notamment due à notre façon d’acheter compulsivement du
matériel et d’en changer sans réel besoin : on change de téléphone en moyenne
tous les 2 ans et d’ordinateur tous les 3 à 4 ans ! Mais je laisse ce sujet à
d’autres, je veux me concentrer sur le sujet du développement logiciel
aujourd’hui.

Lorsqu’on cherche comment est réparti cet impact numérique, c’est la
consommation de vidéos qui ressort principalement. Ainsi, [selon le Shift
Project](https://theshiftproject.org/wp-content/uploads/2018/11/Rapport-final-v8-WEB.pdf),
« _Le visionnage d’une vidéo en ligne de dix minutes disponible dans le “Cloud”
induit par exemple une consommation électrique équivalente à la consommation
propre d’un smartphone sur dix jours_ ». Vous penserez à moi la prochaine fois
que vous regarderez une vidéo de « chatons-trop-mignons » en haute définition
sur Youtube !

On pourra donc commencer à nous questionner sur nos usages un par un, en
commençant peut-être par regarder moins de vidéos et en qualité plus basse, le
but étant à terme d’avoir un usage plus respectueux de la planète. Mais je me
pose quand même une question : suis-je vraiment le fautif dans l’affaire ? Je
veux dire, nous sommes tout de même des millions d’utilisateurs et
utilisatrices à partager une consommation irraisonnée de vidéos en ligne et
nous partagerions un même usage **sans s’être jamais concerté·es** ?
Excusez-moi mais je flaire quand même quelque chose de louche dans l’affaire !
Est-ce que ce ne serait pas plutôt l’outil qui me pousse à l’usage ? Mais
pourquoi ? Qui l’a conçu ? Dans quel but ?

Lorsque je me rends sur Youtube, c’est facile : c’est Google qui a conçu cette
plateforme de divertissement. Mais moi, lorsque j’entends Google, j’ai un petit
drapeau qui se lève dans la tête et qui me dit « attention, Google est l’une
des plus grosses capitalisations boursières et génère la grande majorité de son
chiffre d’affaire à partir de la pub ! » Et pour que la pub génère de l’argent,
il faut des personnes pour la regarder. Idéalement beaucoup. Et qui reste
longtemps. Est-ce que j’ai réellement consenti à regarder beaucoup de vidéos à
la suite… ou est-ce Google qui m’a incité à le faire pour vendre du « temps de
cerveau disponible » (comme dirait l’autre) et donc m’incite à rester sur sa
plateforme ? Vous l’aurez compris, je touche du doigt un problème du
<del>capitalisme</del> design de l’attention.

Donc non seulement il y a des usages, des logiciels qui ne respectent pas
l’environnement, mais en plus ils ne respectent pas non plus leurs
utilisateurices ! Est-ce qu’on ne pourrait pas y faire quelque chose ? Par
exemple, si on commençait à virer cette colonne de suggestions de vidéos « qui
pourraient nous intéresser » ainsi que l’enchainement automatique de vidéos ?
Évidemment Youtube ne vous permet pas de le faire… mais est-ce qu’on aurait pas
inventé une chose qui se nomme « le logiciel libre » ? Si je n’aime pas ce que
fait telle ou telle plateforme de mon attention, alors je vais la modifier pour
qu’elle me respecte, n’est-ce pas la promesse du logiciel libre ?

Nous sommes sauvé·es !<br />
…<br />
Vraiment ?

Prenons une plateforme de vidéos libre comme Peertube, et imaginons qu’il y ait
au sein de Peertube un tel système de suggestions de vidéos ainsi que
l’enchainement automatique de vidéos. Qui dans la salle se sent capable de
modifier le code pour virer ces deux fonctionnalités ? Et en moins d’une heure
(le temps que je considère comme raisonnable pour ne pas avoir l’impression de
le perdre) ? Et bien je suis désolé de vous annoncer que toutes les personnes
qui ont levé la main ne sont pas vraiment libre ! Ils ou elles sont dépendantes
soit d’une personne externe, soit de temps et de ressources qu’elles devront
dépenser de façon disproportionnée vis-à-vis du gain consenti. La liberté 1 du
logiciel libre, celle qui implique que l’on peut modifier le code n’est
finalement peut-être pas si bien respectée.

> Note : quelques mains se sont levées dans la salle lorsque j’ai posé la
> première question, mais toutes se sont rabaissées lorsque j’ai précisé « en
> moins d’une heure ». J’ai bien conscience qu’il s’agit d’un temps arbitraire,
> mais j’avais à cœur de remettre en perspective le « temps disponible » que
> nous n’avons pas toutes et tous à disposition de manière égale.

J’ai donc posé lors de cette partie trois questions / problèmes qui se posent à
moi aujourd’hui dans le numérique : mon usage du numérique respecte-t-il la
planète ? Suis-je maître de mon attention lorsque j’utilise un logiciel ? Et
suis-je capable de modifier ce logiciel pour qu’il respecte l’usage que je
souhaite avoir de lui ?

Mais vous, **quels problèmes vous pose le numérique aujourd’hui et aimeriez
voir disparaître / résolu demain ?**

> Note : la discussion a eu tendance à dévier vers des solutions aux problèmes
> que j’ai soulevés. En cela le résultat de cette première question est mitigé,
> bien qu’il y ait eu des prises de parole vraiment pertinentes. N’ayant pas
> pris de notes, je ne serai pas capable de tout retranscrire ici cependant.

## Cultivons notre jardin (ou une vision positive du numérique de demain)

Bien, maintenant que nous avons établi les problèmes que nous pose le
numérique, on voit un peu mieux ce dont on ne veut pas… mais du coup, on veut
quoi ?

Essayons pour cela de commencer par déterminer ce qui nous fait vibrer. Pour ma
part, mon premier gros coup de cœur pour le numérique a été un jour d’hiver
lorsque j’étais en troisième et que j’ai reçu mon premier <abbr>PC</abbr> à
Noël. Lorsque j’ai réalisé que j’allais pouvoir avoir Internet directement dans
ma chambre, je me suis tout de suite imaginé converser avec des personnes aux
quatres coins du monde. C’est difficile de définir ce que j’ai ressenti mais
je m’en souviens encore. Et c’était synonyme d’un numérique très positif.

Mais bon, à l’époque c’était compliqué l’informatique pour moi, on était encore
en 56K avec le modem qui faisait du bruit dans le bureau de mes parents et
j’étais persuadé qu’il fallait que j’installe le cédérom fourni par Orange pour
que ça marche. Révélation : ça n’a jamais marché. Le temps est passé, Internet
s’est fait omniprésent et me vendait de moins en moins de rêve ; une sorte de
routine s’est installée.

Mais voilà qu’en 2017, avec les copains de [Framasoft](https://framasoft.org/)
on lançait la campagne [Contributopia](https://contributopia.org/). Je me
souviens très bien du sentiment positif que j’ai eu lorsque Pouhiou et pyg nous
ont présenté ce qu’ils avaient en tête. C’était le même sentiment que quelques
années plus tôt, et ça dessinait un numérique tout aussi positif, voire plus.

Ce sont ces sentiments, ces imaginaires que j’aimerais qu’on arrive ensemble à
reconstruire. Si on pouvait appuyer sur un bouton _reset_, il ressemblerait à
quoi notre monde numérique ? Plutôt que de faire une liste détaillée qui
prendrait des plombes à faire, je vais me contenter de parler de trois
« zones » que j’aimerais rebatir si je devais repartir de zéro. Cette liste est
donc très loin d’être exhaustive ! Notez que je vais mélanger alégrement
numérique et Internet dans cette partie, c’est voulu.

Le premier point, c’est celui qui m’a fait vibrer au collège. C’est cette
possibilité de pouvoir faire des voyages culturels, le cul vissé sur ma chaise
et au prix d’une connexion Internet ; rencontrer son prochain sans contrainte
de distance. Peut-être que ça ne fait pas rêver tout le monde, mais ça faisait
rêver un garçon de 13 ans, donc pourquoi pas commencer par là ? Et de toute
façon demain sera sans avion, autant commencer à changer notre façon de
voyager.

Le deuxième sujet numérique qui me donne ce sentiment « wahou ! », c’est le
partage des ressources et des connaissances. Vous connaissez peut-être ce petit
site qui se nomme Wikipédia : il s’agit d’une encyclopédie en ligne alimentée
uniquement par des bénévoles… je vous jure que ça marche ! Et tout le monde
peut participer, donc même des sujets hyper spécifiques peuvent se retrouver
être présentés en long et en large. Ce que j’aime, c’est ce pot commun, tout le
monde contribue et tout le monde peut se servir sans léser personne. Il n’y a
pas d’accaparement de la connaissance, c’est pour tout le monde.

Enfin, le troisième coin d’Internet que je trouve « beau », ce sont les zones
d’information et d’entraide entre oppressé·es et personnes ayant besoin de se
renseigner pour « améliorer » leur cadre de vie, pour s’organiser ou tout
simplement pour discuter. L’anonymat notamment que permet Internet offre la
possibilité de se dégager d’une certaine honte et de se sentir plus en
confiance pour aborder certaines recherches et discussions. Les liens tissés
entre les individus sur Internet sont généralement décrits comme « virtuels »,
mais je vous assure que ça ne l’est pas du tout. Ces liens numériques se
prolongent d’ailleurs bien souvent dans la vie « réelle » (mais je préfère le
terme « vie physique »).

Vous me rétorquerez alors : « ton Internet de demain il ressemble quand même
vachement à celui d’aujourd’hui » et vous aurez sans doute raison. Sauf que
dans celui d’aujourd’hui, ce ne sont pas ces aspects-là qui dominent.
J’aimerais que les logiques de communautés prennent le pas sur les logiques
individuelles en quelque sorte. Et l’élagage des usages que j’ai effectué
permet de rendre le numérique plus soutenable. Mais un réseau d’entraide basé
sur des vidéos en très haute qualité n’est pas forcément beaucoup plus
soutenable, il faut bien évidemment se poser aussi la question de la conception
en plus de celle des usages.

Alors comment on conçoit pour demain ? Quel est le secret si bien gardé de
cette conférence ?

Je vous propose de partir sur une idée que j’applique de temps en temps lorsque
je veux progresser dans ma pratique d’un sujet : je me pose des contraintes
fortes. Ces contraintes ne sont pas des axiomes de conception qu’il faudrait
appliquer coûte que coûte, mais des exercices qui doivent vous pousser à vous
questionner sur des pratiques que vous auriez plus ou moins intégrées comme
« normales ». Ces contraintes doivent répondre à un problème que vous cherchez
à résoudre. Et ça tombe bien, on a identifié au moins trois problèmes dans la
partie précédente !

Concernant l’impact environnemental tout d’abord, quel genre de contraintes
peut-on imaginer ? Le poid des pages étant souvent pointé du doigt, commençons
par là. Première contrainte : transformer un site ou une application pour que
ses pages de fassent pas plus de 200Ko, images comprises ! Cette première
contrainte devrait déjà soulever des questions : est-ce que mon fichier HTML
est suffisamment simple ? Et mon fichier CSS ? Mais cette librairie JavaScript
pèse déjà 200Ko, quelles implications si je dois m’en passer ? Combien d’images
puis-je insérer ? Avec quelle qualité ? Et comment appliquer ça à un service de
partage de photos ou de vidéos ? Quels choix de conception cela implique-t-il ?
Peut-être que vous ne trouverez pas de réponse satisfaisante sans relacher
légèrement la contrainte, mais ce n’est pas grave car vous aurez déjà commencé
à vous poser la question et imaginer des solutions plus « simples ».

Sur l’attention de l’utilisateurice maintenant. Je rappelle que son but dans la
vie n’est pas de passer son temps sur votre site ou application, il ou elle a
bien d’autres chats à fouetter (mais ne faites pas ça chez vous, c’est de la
maltraitance animale et c’est mal). Imaginons la contrainte suivante : au bout
de 5 minutes passées (durant une journée) sur votre application, celle-ci se
ferme automatiquement. Comment allez-vous la concevoir pour que
l’utilisateurice fasse tout de même ce qu’elle est censée faire ? Cela pose la
question de ce que doit faire votre application car il vous faut maintenant
définir quelles fonctionnalités sont maintenant essentielles. Par exemple,
comment appliquer cette contrainte à un réseau social ? Quels choix allez-vous
faire pour que la personne qui se rend sur Twitter ou Mastodon puisse toujours
lire ce qu’il ou elle l’intéresse ? Et au fait, à quoi ça doit servir un réseau
social ? Faire de la veille ? se détendre ? créer des liens ? organiser des
communautés ? tout à la fois ?

Enfin, concernant la maintenabilité des logiciels, comment favoriseriez-vous la
possibilité pour un néophyte de rentrer dans votre code ? Exemple de contrainte :
votre programme doit faire moins de 500 lignes de code. Là encore peut-être
qu’il va devenir compliqué de recréer quelque chose comme un réseau social ou
une plateforme de partage de vidéos, mais posez-vous les questions que cela
engendre. Que doit faire mon programme ? Comment j’organise mon code ? Vous
réaliserez sans doute qu’on ne peut pas faire l’impasse sur une « éducation au
code » et tant mieux. Maintenant : quelles sont les actions que vous prendrez
pour qu’un minimum d’éducation soit nécessaire pour au moins comprendre
l’architecture de votre logiciel ? Et on voit que des tas de questions
peuvent découler d’une bête question (celle de rendre votre code accessible au
plus grand nombre).

On peut évidemment imaginer des tas d’autres contraintes comme limiter le temps
de chargement de votre site ou application à moins de 0,5 seconde, limiter le
nombre d’écrans à 2, développer une application sans aucune dépendance
autres que celle du langage et de sa librairie standard ou encore limiter
l’accès au réseau au strict minimum. Vous pouvez aussi les mixer, les modifier,
etc. Ces contraintes vont sans doute vous paraître contre-productives par
moment, voire totalement opposées à leur objectif et c’est pour ça que
j’insiste sur le fait qu’il ne s’agit que de guides de conception. Le risque
étant d’appliquer aveuglément ces contraintes en perdant de vue votre objectif
initial.

En appliquant ces différentes contraintes, j’ai bon espoir que vos logiciels
deviennent plus simples, plus respectueux d’un environnement mal en point et
d’utilisateurices souvent malmené·es. _In fine_, ce sont vos logiciels qui
deviendront plus libres. Pour rappel, ce n’est pas vraiment le logiciel qui
doit être libre (lui il s’en fout, vous pouvez lui demander mais il y a peu de
chances qu’il réponde), c’est son utilisateur ou utilisatrice. Alors la
prochaine fois que vous concevrez un logiciel, merci de penser à sa liberté à
ellui.

Et vous, **quels usages vous semble prendre tout leur sens dans le numérique ?
Qu’est-ce qui vous fait vibrer là-dedans ? Et comment le concevriez-vous ?**

## Conclusion

J’ai évoqué à un moment donné que je faisais parti de l’association Framasoft.
Bien que les questions posées lors de cette conférence et les esquisses d’un
monde numérique plus sobre fassent parties des réflexions qu’on mène dans
l’asso et plus spécifiquement dans le cadre de notre campage Contributopia, je
voulais souligner le fait que le sujet ne nous appartient pas. À partir du
moment où vous vous sentez concerné·e, alors vous êtes légitime pour traiter le
sujet et probablement tout aussi pertinent·e que nous. Je voulais faire ce
petit _aparte_ car il semble y avoir de plus en plus d’attentes envers
l’association pour porter ces réflexions. Cette confiance nous fait certes
plaisir, mais notre monde à nous (celui de demain) est émancipé et ne repose
pas sur un groupe de quelques copain·ines. Alors n’hésitez surtout pas à
cogiter sur tout ça dans votre coin et à lancer ensuite d’autres structures
collectives pour imaginer d’autres imaginaires. C’est en les multipliant qu’on
avancera !
