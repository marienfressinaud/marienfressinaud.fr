---
title: Redirections
date: 2022-01-31 17:40
---

J’ai migré en 2016 mon blog de [PluXml](https://pluxml.org/) vers [Pelican](https://blog.getpelican.com/) (avant une nouvelle migration vers [Boop!](https://framagit.org/marienfressinaud/boop) en 2019). Si j’avais bien pensé à rediriger les adresses des flux <abbr>RSS</abbr> à l’origine (ex. `/feed` ou `/feed.php`), les redirections ont fini par se perdre au gré des mises à jour. Je viens de les remettre en place. Re-bienvenue aux personnes qui découvriront une ribambelle de nouveaux articles 👋

En plus des flux <abbr>RSS</abbr>, j’ai eu un [Shaarli](https://shaarli.readthedocs.io) pendant un certain temps. Celui-ci a fini par disparaître, mais je continuais à recevoir quelques visites à son ancienne adresse (en erreur 404 depuis longtemps). Désormais, cette adresse sera redirigée vers [mon profil Flus](https://app.flus.fr/p/1670839367044869607).

Enfin, j’ai pris le temps de rediriger les sous-domaines `www.` et `next.` (utilisé durant la conception de cette version du site) vers le nom de domaine de base.