---
title: Le fichier GDPR.txt
date: 2022-02-23 19:28
update: 2022-03-02 22:05
---

Dans le cadre de mon bénévolat pour [Framasoft](https://framasoft.org/), je suis amené à travailler sur la conformité de l’association au [Règlement Général sur la Protection des Données](https://www.cnil.fr/fr/comprendre-le-rgpd) (<abbr>RGPD</abbr>). Ça fait des années qu’on essaye d’avancer sur le sujet, mais il faut bien reconnaître que, vu le nombre de services et d’activités qu’on gère, c’est la grosse galère. D’ailleurs, si ça vous étonne, on a une entrée claire [dans la <abbr>FAQ</abbr>](https://contact.framasoft.org/fr/faq/#gpdr).

On a quand même bien avancé depuis octobre dernier, accompagné‧es que nous sommes par [stella.coop](https://www.stella.coop/). Notre liste des traitements est sur pied, mais il nous reste à compléter les fiches des différents traitements (on en documente d’ailleurs l’avancement [publiquement](https://wiki.framasoft.org/rgpd)). Il nous faut donc passer sur chaque service, déterminer les finalités des traitements, lister les données récupérées, vérifier si elles sont obligatoires, leur visibilité, etc.

Face à cette montagne, une idée me trotte dans la tête depuis un moment : **ce serait pratique (et bien sympa de leur part) que les développeurs et développeuses de logiciels proposent elles-mêmes un document synthétisant les informations utiles dans le cadre du <abbr>RGPD</abbr>.** Ça faciliterait le travail de celles et ceux qui proposent des instances de leur logiciel, ça mutualiserait le travail ingrat et ça limiterait les erreurs.

Comme on n’est jamais aussi bien servi que par soi-même, je teste actuellement un format dans le dépôt de [flusio](https://github.com/flusio/flusio) : **le fichier GDPR.txt.**

Le format est simple : un système `clé: valeur` organisé par blocs, chaque bloc étant séparé des autres par une ligne vide. Il existe deux types de blocs : celui pour indiquer les finalités et celui pour indiquer les données collectées.

Une finalité est décrite par les clés suivantes :

- `purpose` : la finalité du traitement de données (champ libre) ;
- `lawfulness` : la base légale du traitement (<i lang="en">consent, contract, legal, public interest, legitimate interest, vital</i>, cf. [le site de la CNIL](https://www.cnil.fr/fr/les-bases-legales/liceite-essentiel-sur-les-bases-legales)).

Une donnée est décrite par les clés suivantes :

- `data` : le nom de la donnée collectée (champ libre) ;
- `required` : si la donnée est obligatoire ou non (`true` ou `false`) ;
- `visibility` : par qui est accessible la donnée au sein de l’application (champ libre, ex. <i lang="en">private</i>, <i lang="en">public</i>, <i lang="en">administrators</i>) ;
- `description` : pour expliquer comment est utilisée la donnée (champ libre) ;
- `mitigation` : pour détailler les mécanismes qui rendent la donnée moins vulnérable (champ libre facultatif, ex. « données supprimées après X jours », « pseudonyme autorisé »).

Si le format vous intéresse, j’ai créé [un dépôt Framagit pour en discuter et améliorer les choses publiquement](https://framagit.org/marienfressinaud/gdpr-txt). **Je crois que ce fichier est plus qu’utile et faciliterait la conformité <abbr>RGPD</abbr> pour beaucoup de monde.**

---

**Mise à jour du 02 mars.**

J’ai finalement publié tout ça sous la forme d’un site dédié, [gdpr-txt.org](https://gdpr-txt.org) et [d’un dépôt sur Framagit](https://framagit.org/marienfressinaud/gdpr-txt) pour contribuer.
