---
title: Sélection d’articles
date: 2022-03-06 19:00
---

J’ai fait le choix de ne pas lister tous mes articles sur [la page du blog](blog.html). La liste est limitée à un maximum de 20 articles et remonte jusqu’à 6 mois en arrière.

Certains articles plus vieux méritent tout de même de rester en première page, parce que je les aime bien, ou parce qu’ils marquent une étape importante dans mon parcours. Je veux garder cette sélection à un maximum de 10 articles.

Jusqu’ici, tout allait bien. Mais depuis que [je publie plus souvent](publier-souvent.html), je me demande si je ne vais pas avoir besoin de critères supplémentaires… ou être plus souple sur la limite ?