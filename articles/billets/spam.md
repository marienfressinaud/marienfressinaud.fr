---
title: Spam
date: 2022-02-13 23:59
---

Je ne sais pas ce qu’il se passe en ce moment, mais ça semble être la foire aux spams.

Ça a commencé avec les appels [d’arnaque au Compte Personnel de Formation](https://www.moncompteformation.gouv.fr/espace-public/attention-arnaques-cpf-soyez-vigilant) à partir de fin décembre.

Ça a continué avec des boîtes qui m’envoient de la pub à mon domicile sous couvert que mon adresse postale est une adresse professionnelle. Fonctionne évidemment aussi avec mes adresses courriel liées à Flus, même celles qui sont purement techniques.

Sont venus d’autres numéros qui m’appellent régulièrement. Cette semaine, un numéro a tenté de me joindre quasiment tous les jours. Je l’ai bloqué.

Aujourd’hui, ce sont 260 courriels que mon serveur a rejeté (contre 45 sur l’ensemble du reste de la semaine). Ce sont les quelques faux négatifs qui m’ont mis la puce à l’oreille. Heureusement que j’ai mis sur pied un blocage par nom de domaine il y a quelques semaines.

Je ne sais pas ce qu’il se passe en ce moment, mais ça commence légèrement à m’agacer…