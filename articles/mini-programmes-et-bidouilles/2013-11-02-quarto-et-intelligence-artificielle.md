Date: 2013-11-02 12:35
Title: Quarto et intelligence artificielle
Slug: quarto-et-intelligence-artificielle
Tags: ia, python
Summary: En 2013 je suis parti pour un trimestre dans la paisible ville de Trondheim, en Norvège. Là-bas, j’ai pris un cours d’intelligence artificielle particulièrement ludique. En est ressorti trois programmes écrits en Python, [disponibles sur GitHub](https://github.com/marienfressinaud/AI).

Maintenant que j’ai à peu près fini de réparer mon serveur, je prends un peu de temps pour partager le code source d’un projet que j’ai réalisé pour mon cours de programmation d’Intelligence Artificielle (IA) en septembre : un jeu basique de Quarto.

Pour présenter le Quarto, rien de mieux que de [citer Wikipédia](https://fr.wikipedia.org/wiki/Quarto) :

> L’objectif : aligner quatre pièces ayant au moins un point commun entre elles. Mais ne croyez pas que vous jouerez celles que vous voudrez : c’est l’adversaire qui choisit pour vous !

Les seize pièces du jeu, toutes différentes, possèdent chacune quatre caractères distincts : haute ou basse, ronde ou carrée, claire ou foncée, pleine ou creuse. Chacun à son tour choisit et donne une pièce à l’adversaire, qui doit la jouer sur une case libre. Le gagnant est celui qui, avec une pièce reçue, crée un alignement de quatre pièces ayant au moins un caractère commun [...]

Je ne sais pas pour vous, mais je trouve le principe très sympa :) Une sorte de puissance 4 mais avec quelques contraintes supplémentaires. Vous pouvez [le tester directement en ligne](http://quarto.freehostia.com/fr/).

Mon projet, quant à lui, se focalisait sur l’aspect IA et je me suis contenté du strict nécessaire niveau interface utilisateur. L’objectif était d’implémenter [l’algorithme Minimax](https://fr.wikipedia.org/wiki/Algorithme_minimax) avec [optimisation alpha-bêta](https://fr.wikipedia.org/wiki/%C3%89lagage_alpha-beta).

Je crois que j’ai fait quelque chose de pas trop mal mais la fonction d’évaluation (qui est censée donner une "note" à un coup) est loin d’être parfaite et c’est là que j’ai perdu des points. Le code n’est pas non plus spécialement propre, mais il reste lisible (c’est du Python en même temps :p).

Le code est disponible [sur Github](https://github.com/marienfressinaud/AI_quarto) et est sous licence MIT. J’ai accompagné le code source d’un diagramme de classes très général pour montrer l’architecture globale du jeu.
