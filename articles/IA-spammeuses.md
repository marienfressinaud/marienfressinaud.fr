---
title: Réflexions autour des IA spammeuses
date: 2023-03-26 20:30
---

À l’inverse d’autres technos comme les <abbr>NFT</abbr> ou le Metaverse, j’ai le sentiment que nous n’échapperons pas au raz-de-marée des technologies basées sur de l’Intelligence Artificielle.
Nous n’échapperons pas non plus aux dégâts qu’elles occasionneront.
Je note ici quelques réflexions que j’ai partagé en interne sur le Framateam de Framasoft.

J’ai observé depuis quelques semaines du côté de [FreshRSS](https://freshrss.org) une recrudescence de tickets faisant des demandes à priori légitimes.
Ces tickets ont été créés par des utilisateurs dont le compte GitHub a disparu quelques jours plus tard (exemples [1](https://github.com/FreshRSS/FreshRSS/issues/5126), [2](https://github.com/FreshRSS/FreshRSS/issues/5117), [3](https://github.com/FreshRSS/FreshRSS/issues/5115), [4](https://github.com/FreshRSS/FreshRSS/issues/5114)).
Certains ont clairement des comportements bizarres, voir notamment [celui qui m’a amené à me poser ces questions](https://github.com/FreshRSS/xExtension-HelloWorld/issues/1).
Le ticket se trouve dans le mauvais dépôt, le titre _semble_ répondre à [un autre ticket](https://github.com/FreshRSS/FreshRSS/issues/4295) du dépôt principal, mais le contenu est incohérent.
D’autres tickets sont plus cohérents, mais restent « étranges ».
Quoi qu’il en soit je n’avais encore jamais vu autant de comptes différents disparaître aussi vite qu’ils sont apparus.
Je me demande si ce n’est pas GitHub qui les a supprimés.

J’ignore si ces comptes étaient gérés par des bots.
Le problème est que cela aurait pu l’être.
Quoi qu’il en soit, ça m’a amené à penser que les technologies comme <abbr>GPT</abbr> vont ouvrir grandes les portes au spam dans le moindre interstice d’Internet.
Avec l’évolution de la technologie, ça va devenir de plus en plus compliqué de faire la distinction entre un compte légitime et un compte de spammeur.
Au point, j’imagine d’épuiser les mainteneurs de projets communautaires (dans le cas qui m’intéresse en l’occurrence, mais potentiellement toute personne amené à lire / réfléchir / réagir à du contenu posté par une autre personne).

J’ai également de gros doutes sur la capacité des anti-spams actuels (ex. Rspamd, Akismet) à détecter du spam généré par une <abbr>IA</abbr>.

Enfin, je pressens une perte de confiance dans le contenu posté par autrui : si je ne sais pas si je fais face à une véritable personne intéressée par mes propos ou à un bot qui veut me faire tourner en bourrique, combien de temps vais-je tenir avant de me désintéresser des interactions sur le Web ?
