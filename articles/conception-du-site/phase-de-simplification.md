---
title: Phase de simplification
date: 2019-09-11 10:15
---

J’ai la mauvaise habitude de tenter de me compliquer la vie (c’est humain
j’imagine). La conception de ce site en est un bon exemple. Après quelques mois
de recul sur ce que j’ai réalisé, je me suis rendu compte que [la page d’accueil](index.html)
notamment ne collait pas du tout avec ce que je voulais mettre en valeur. J’ai
pris 30 minutes ce matin pour changer tout ça, en me prenant moins la tête. Je
vais également me simplifier la vie et rédiger des articles (beaucoup) moins
longs. On verra bien si ça me motive à en écrire plus.
