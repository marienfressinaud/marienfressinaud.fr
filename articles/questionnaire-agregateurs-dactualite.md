---
title: Questionnaire sur votre utilisation des agrégateurs d’actualité
date: 2019-05-29 17:50
---

J’ai partagé hier matin sur les réseaux sociaux un questionnaire pour étudier
l’utilisation des agrégateurs d’actualité (ou « de flux <abbr>RSS</abbr> »). Je
reviendrai plus longuement dans un autre article sur le pourquoi et le comment
mais, pour faire court, je souhaite me mettre à mon compte d’ici la fin de
l’année et j’ai pour cela besoin de connaître un peu mieux les attentes des
personnes utilisant de tels outils.

J’ai pour le moment récupéré plus de 200 réponses, ce qui représente le double
de ce que j’espérais initialement. J’aurais toutefois souhaité un peu plus de
réponses de personnes prêtes à payer pour un tel service (un peu plus du tiers
des répondants pour le moment).

Si vous souhaitez me donner un petit coup de main, n’hésitez donc pas à remplir
[ce questionnaire](https://framaforms.org/votre-utilisation-des-agregateurs-dactualite-1558172477)
et à **le partager auprès de vos proches et collègues**. Je l’ai pensé court
pour pouvoir le remplir en 5 minutes sans avoir à se creuser la tête. Je ferai
sans doute un questionnaire plus détaillé ensuite pour les personnes acceptants
de prendre un peu plus de temps.

Je publierai l’analyse des résultats sous la forme probable d’un article de
blog afin que cela puisse bénéficier à n’importe qui.

Merci d’avance !
