---
title: Vu, lu, joué (janvier 2022)
date: 2022-01-30 19:25
---

Vu [Belle](https://fr.wikipedia.org/wiki/Belle_(film,_2021)) de Mamoru Hosoda, réinterprétation de La Belle et la Bête au sein d’un monde « virtuel ». Magnifique pour les yeux et les oreilles, la fin m’a scotché malgré quelques grosses ficelles scénaristiques.

Lu [Bolchoi Arena](https://fr.wikipedia.org/wiki/Bolchoi_Arena)  — tome 3 — Révolutions qui explore, ici aussi, un monde « virtuel ». Peut-être 2 – 3 moments où j’ai eu du mal à suivre l’intrigue, mais on retombe sur nos pattes. J’aime tellement le style du dessin ♥️

Joué à [<i lang="en">Animal Crossing: New Horizons</i>](https://fr.wikipedia.org/wiki/Animal_Crossing:_New_Horizons) sur Switch une bonne partie du mois. C’était ce dont j’avais besoin en ce début d’année. J’ai découvert au passage le terme de « [<i lang="en">wholesome</i>](https://www.youtube.com/watch?v=IgJ06XhvIgI) » pour décrire ce type de jeux.
