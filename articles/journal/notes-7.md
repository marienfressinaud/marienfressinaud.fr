---
title: Notes #7
date: 2020-03-23 10:30
blog: false
---

Mon confinement se passe « bien » (même si ça pèse un peu par moment, je n’ai
pas mis un pied dehors depuis dimanche 15 mars). J’ai l’habitude de bosser de
chez moi. En théorie cela n’aurait pas vraiment dû impacter mon rythme de vie,
mais j’ai quand même été saisi d’un état de sidération face aux informations
liées au coronavirus et ses impacts.

Pour en sortir j’ai décidé de ne pas allumer le PC les matins de la semaine
dernière pour prendre du temps pour moi : lecture, dessin, ménage et pain. Ça
m’a fait du bien et je me sens plus apte à démarrer cette semaine.

---

Qui dit coronavirus dit annulation des événements. Par conséquent [les
JdLL](https://jdll.org/) de 2020 ont été annulés. Je suis triste à la fois
parce que c’est une chouette occasion de (re)croiser des personnes que
j’apprécie, et pour les bénévoles qui organisent tout ça.

De mon côté j’ai hésité à quand même préparer la conférence que j’avais prévue
de donner, mais je vais finalement profiter de ce temps libéré pour bosser sur
la phase 2 de Flus.

---

Je suis curieux de savoir comment cette phase 2 de Flus va être accueillie. Si
je suis confiant dans les fonctionnalités imaginées, mon mode de fonctionnement
va largement différer d’avant.

On me demande régulièrement d’avoir une avant-première de ce que je vais
annoncer. Pour l’instant je réponds que « c’est trop tôt » à la fois pour ne
pas m’enfermer dans des fonctionnalités que je pourrais juger, à terme,
non souhaitables, et pour ne pas avoir à donner de date. M’engager sur une date
signifie pour moi une augmentation de la pression et donc du stress. Je n’ai
pas envie de ça.

Ceci dit je garde dans un coin de ma tête ces demandes. Je donnerai le lien
vers la refonte de la page d’accueil un peu plus tôt aux personnes me demandant
cette « avant-première ».

---

J’ai souvent envie de partager ma veille ici, sur ce blog. Problème : je n’ai
pas le réflexe de garder les liens et je ne sais pas trop comment la présenter.
C’est un des problèmes que je veux résoudre avec Flus.

---

Je n’avais pas essayé de dessiner depuis environ un an. Ça me prend comme ça,
par période. On va dire que c’est l’idéal en ce moment.

Comme je commence à identifier ce qui me décourage à chaque fois, je me mets un
peu moins de pression. J’arrive à réduire mes exigences vis-à-vis de moi-même
et à prendre du plaisir à dessiner des objets simples, comme un bloc de
post-it (y’a déjà tellement de choses à y observer !)

J’aime cette sensation d’apprendre de (presque) zéro et de sentir que je me
libère de la pression que je me mets tout seul. C’est la deuxième fois que je
ressens ça aussi clairement, après la confection du pain :)
