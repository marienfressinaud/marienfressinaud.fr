---
title: Notes #9
date: 2020-04-27 11:21
blog: false
---

J’ai enfin lancé le nouveau site [flus.fr](https://flus.fr)&nbsp;! Je suis très
content du résultat, j’ai rarement autant peaufiné un site dans ses moindres
détails (rédaction, performances, optimisations, etc.) Les retours sont
également positifs et [la cagnotte](https://flus.fr/cagnotte) permettra déjà de
débloquer au moins 50 abonnements mensuels lors de la sortie du service.

Encore des petites choses à travailler par-ci par-là avant de me lancer
réellement dans la conception de ce nouveau logiciel, mais je vais pouvoir
rapidement commencer.

---

Je me force désormais à traiter les courriels au fur et à mesure que je les
reçois afin d’être plus réactif dans mes réponses. C’est une tentative, je
serai vigilant pour que ça ne se transforme pas en une source d’angoisse.
Jusqu’à maintenant, c’est plutôt positif.

---

Je tente depuis la semaine dernière de me limiter à 6h de travail par jour et
1h supplémentaire de détente sur l’ordinateur. Ça me libère l’esprit et me
donne du temps pour faire du dessin. Je me demande quand même si ça me laisse
assez de temps pour avancer autant que je le souhaite sur Flus, c’est parfois
assez frustrant. Comme d’hab’, j’expérimente et j’ajusterai.

D’une manière générale, j’ai l’impression d’enfin apprendre à «&nbsp;travailler&nbsp;».
Durant mes études je me suis grandement reposé sur mes facilités et je me rends
compte aujourd’hui que les méthodes que je mets en place aujourd’hui m’auraient
été grandement utiles plus tôt. Il n’est jamais trop tard pour apprendre&nbsp;:)

---

Ça va bientôt faire un mois que je dessine quotidiennement, sans interruption.
C’est parfois 10 minutes, parfois plusieurs heures, généralement au moins 30
minutes. Je commence enfin à m’y mettre de façon un peu automatique en fin de
journée, et il est certain que mon nouveau rythme de vie y participe
grandement.

Je discerne mieux ce qui me permettra de progresser pendant les prochains mois,
mais je réalise d’autant plus la quantité de travail que cela requiert pour
devenir «&nbsp;bon&nbsp;». Cela interroge forcément sur l’objectif que je me
donne… et pour l’instant je n’en ai pas de bien précis. Je tâtonne, et c’est
très bien ainsi.

---

J’ai toujours été frappé de la manière que les artistes professionnel·les ont
d’être hyper critiques vis-à-vis de leur travail. Jusque-là ça avait le don de
me frustrer car je me disais «&nbsp;si cette personne trouve que son travail
est nul alors que je ne vois pas de défaut, que peut bien valoir mon propre
travail&nbsp;?&nbsp;»

J’ai pris conscience seulement cette semaine qu’on se place tous dans un
référentiel d’expériences différent. La critique que l’on porte à son propre
travail **doit** prendre en compte ce référentiel. Bien sûr j’en avais
conscience intuitivement, mais c’est une chose différente que de le réaliser
concrètement. Merci à [Yeah Cy.](https://www.youtube.com/channel/UCdnLGVqAi5QIQ2SunCsEsDA)
qui a trouvé des mots pour que je réalise ça.

---

La pluie, [enfin](https://www.ledauphine.com/edition-grenoble-vercors/2020/04/26/un-record-de-42-jours-sans-pluie).
