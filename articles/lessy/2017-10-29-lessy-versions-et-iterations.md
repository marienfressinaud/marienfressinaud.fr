Date: 2017-10-29 10:00
Title: Lessy : versions et itérations
Slug: lessy-versions-et-iterations
Tags:
Summary: Avec Lessy, je souhaite expérimenter une autre manière de sortir des versions de mon application&nbsp;: des cycles courts pour des nouveautés plus régulières.

Je me suis intéressé ces derniers jours au système de sortie des nouvelles
versions de [Lessy](https://github.com/marienfressinaud/lessy). Cela peut
paraître étonnant, mais décider d’un tel système est parfois compliqué et pose
plusieurs problèmes.

Pour commencer, on peut se poser la question «&nbsp;pourquoi versionner&nbsp;?&nbsp;».
J’y vois personnellement quatre intérets en particulier&nbsp;:

- montre que le projet vit (**marketing**)&nbsp;;
- donne une indication sur le niveau de modifications au sein du logiciel
  depuis la version précédente (**dépendances**)&nbsp;;
- permet de revenir à une version plus ancienne en cas de soucis avec la
  nouvelle (**sécurité**)&nbsp;;
- donne l’occasion de célébrer quelque chose au sein de la communauté
  (**motivation**).

Lorsque je travaillais encore sur [FreshRSS](https://freshrss.org), nous nous
étions souciés de la question de la visibilité. Nous avions alors trois
versions distinctes&nbsp;: une stable, sortant à intervalles plutôt longs, la
bêta qui sortait tous les… hum… deux mois (?) et la version de développement.
Nous avions décidé de mettre en avant la version bêta qui était alors moins
testée que la version stable mais qui avait l’avantage de garder les
utilisateurs accrochés à «&nbsp;l’actualité&nbsp;» de FreshRSS. Ce système
avait toutefois **le désavantage d’être lourd à maintenir**. Je souhaitais par
conséquent un système plus simple pour Lessy tout en continuant de mettre en
avant le travail quotidien effectué.

Concernant le deuxième point, à savoir que le versionnage donne une indication
sur le niveau de modifications depuis la version précédente, je pense
évidemment au système «&nbsp;[semantic versioning](http://semver.org/)&nbsp;».
Si ce système possède **un intéret évident dans le cas de dépendances** (ex. le
logiciel A dépend du logiciel B dans sa version X), je ne le vois pas dans le
cas d’un logiciel en «&nbsp;bout de chaine&nbsp;» tel que Lessy. C’est pourquoi
je suis parti sur un système de nommage totalement différent.

La raison consistant à donner des numéros de version pour permettre de revenir
en arrière en cas de soucis me paraît évidemment légitime bien que le cas se
présente à mon avis de façon assez rare.

Enfin, sortir une nouvelle version est loin d’être uniquement une tâche
technique. Il s’agit avant tout de **fêter une nouvelle étape franchie dans le
développement** et de marquer d’une pierre blanche l’avancement pour pouvoir
constater plus tard tout le chemin parcouru. Célébrer une nouvelle version,
**ça motive énormément**&nbsp;!

Concrêtement, comment cela va-t-il se passer pour Lessy&nbsp;?

L’ensemble des tâches à effectuer (fonctionnalités, tâches techniques,
corrections de bugs, etc.) sont listées en vrac dans [**les tickets GitHub**](https://github.com/marienfressinaud/lessy/projects),
ce qui me sert par conséquent de boite d’entrée. Ces tâches sont ensuite triées
par petits groupes **au sein d’itérations** représentées par [les projets
GitHub](https://github.com/marienfressinaud/lessy/projects). Les tâches d’une
itération forment un ensemble de fonctionnalités ou tâches techniques
**similaires** et leur temps de développement total effectif ne doit pas être
excessif (je me fixe 2 ou 3 semaines comme maximum). Ces itérations permettent
ainsi de dessiner une pseudo feuille de route du projet. Et comme chacune
d’entre elles est nommée ([d’après les constellations astronomiques](https://fr.wikipedia.org/wiki/Liste_des_constellations)),
**la fin d’une itération donne son nom à une nouvelle version de Lessy**.

Ainsi, des quatre points que je listais plus haut, seule la problématique des
dépendances n’est pas du tout adressée. Pour les trois autres&nbsp;:

- marketing&nbsp;: des versions sortent régulièrement et leurs noms sont plus
  sympas qu’un simple nombre&nbsp;;
- sécurité&nbsp;: revenir à une version antérieure est évidemment toujours
  possible, renforcé par le fait que chaque «&nbsp;commit&nbsp;» dans
  «&nbsp;master&nbsp;» peut être considéré comme stable&nbsp;;
- motivation&nbsp;: savoir qu’une itération est courte par principe permet de
  savoir qu’à la fin du mois je pourrais éventuellement passer à autre chose.
  De plus sortir une version est un processus relativement simple.

Pour terminer, si j’ai choisi de nommer les versions de Lessy par les noms des
constellations astronomiques, c’est avant tout parce que je cherchais un
système de nommage original permettant de penser à autre chose qu’à seulement
du code. Comme lever le nez du clavier ne suffit pas toujours à se changer les
idées, je me disais que le lever vers le ciel serait une bonne idée.
