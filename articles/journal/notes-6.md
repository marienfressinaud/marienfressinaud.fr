---
title: Notes #6
date: 2020-03-09 18:45
blog: false
---

J’avance bien sur Flus. Genre vraiment bien. Il m’aura fallu 6 semaines pour
passer d’un « mais je vais où ? je m’éparpille et je n’avance plus » à une
vision d’ensemble précise. J’ai l’impression de répondre à beaucoup de
problèmes existants dans la veille en ligne et ça va être chouette si j’arrive
à faire ce que je veux. Il me reste à valider mes idées en les confrontant aux
vécus variés des personnes pour faire marcher l’intelligence collective (ce qui
a d’ailleurs déjà très bien fonctionné jusque-là).

---

L’article que j’ai publié hier (« [La low-tech existe-t-elle dans le
numérique](low-tech.html) ») m’aura pris plus de 6h à écrire. Je me rends
compte que je prends beaucoup de plaisir à rédiger ce genre de billets ; par
contre, qu’est-ce que ça bouffe comme temps ! Je ne me vois pas mettre la même
énergie toutes les semaines.

Les retours sur les réseaux sociaux ont été très gratifiants. Ça me fait
réfléchir à comment intégrer un système de gratification au sein de Flus… et
s’il devrait y avoir un tel système.

---

Même si je me couche bien plus tôt qu’il y a quelques années, ça ne fait que
quelques jours que j’essaye de m’imposer des horaires fixes (arrêt du PC 21h30,
couché 22h30, debout 7h). Je ne sais pas si c’est l’effet du changement qui
fait ça, mais je me réveille généralement à 6h30, ce qui me donne l’impression
de faire une grasse matinée. Je me lève ensuite sans soucis et en pleine forme,
par contre je ressens un coup de barre en début d’après-midi.

Ces horaires s’inscrivent dans une démarche pour essayer d’améliorer mon cadre
de vie (et donc _in fine_ ma santé). Mon appartement n’a jamais été aussi bien
rangé et propre. Prendre soin de soi, c’est aussi prendre soin de son
environnement.

---

Pour la rédaction de mes « Notes », j’essaye de me fixer une heure de
publication, l’idée étant de ne pas non plus y passer des plombes.

Ça ne marche pas 😬

---

Pas de « Notes » la semaine dernière : ce n’est pas que je n’avais pas
envie, mais ça m’a permis de me rappeler que le rythme hebdomadaire n’est pas
une règle. J’ai eu l’impression d’avoir plus de plaisir aujourd’hui à me lancer
dans l’exercice que la fois d’avant.
