Date: 2015-03-07 10:06
Title: freshrss.org se dote d’un blog !
Slug: freshrss-org-se-dote-dun-blog
Tags: php, freshrss
Summary: FreshRSS est le premier projet conséquent que j’ai porté. Initié en novembre 2012, la version 1.0 est sortie début 2015 seulement. J’annonçais peu après que je donnais les clés à la communauté pour reprendre le code [disponible sur GitHub](https://github.com/FreshRSS/FreshRSS). J’ai toutefois aujourd’hui toujours des idées pour l’améliorer.

Je l’avais annoncé dans l’article présentant FreshRSS 1.0, c’est désormais chose faite : freshrss.org possède son propre blog !

Par conséquent :

* Cet article sera le dernier de ce blog parlant de FreshRSS ;
* Cet article sera le dernier en français à propos de FreshRSS ;
* Le flux RSS à suivre devient [http://freshrss.org/feeds/all.atom.xml](http://freshrss.org/feeds/all.atom.xml) ;
* Le processus d’écriture devient plus communautaire puisque les articles seront annoncés [sur GitHub](https://github.com/FreshRSS/freshrss.org/issues?q=label%3AArticle) et pourront être rédigés à plusieurs.

Pour plus de détails, vous pouvez vous rendre sur [le premier article paru](http://freshrss.org/freshrss-gets-a-new-website.html) !

Si vous avez des retours à faire, vous pouvez les faire [sur GitHub](https://github.com/FreshRSS/freshrss.org/issues).

Désormais les articles publiés ici seront beaucoup plus personnels, probablement liés dans un premier temps à mes voyages en France puis en Europe, sûrement peu à peu à mes projets d’écriture aussi. Le temps en jugera :).
