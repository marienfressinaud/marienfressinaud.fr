---
title: Notes #3
date: 2020-02-10 10:00
blog: false
---

J’ai découvert que pour pouvoir faire un virement vers un autre compte bancaire
depuis Internet chez la Banque Postale, il me fallait activer une option.
Option qui doit être activée en remplissant un document PDF. Document PDF dont
le lien ne m’est pas donné. Je suis donc allé en agence me disant que j’allais
gagner du temps. C’était sans compter qu’une fois sur place, aucune personne
n’était habilitée à le faire car il s’agit d’un compte « entreprise ».

J’ai vraiment du mal à concevoir que ce genre de choses soient aussi
compliquées à réaliser aujourd’hui. Je suis partagé entre mon envie
consommateur-centré de pouvoir bénéficier d’un service sans anicroche, et une
certaine forme de relativisme : tout ça n’est pas si grave.

---

Je n’ai rien publié depuis 2 semaines sur le « [carnet de flus](https://flus.fr/carnet) ».
Ça m’embête un peu car je m’étais plus ou moins engagé à un rythme hebdomadaire,
et en même temps je n’ai pas envie de me l’imposer si je n’ai pas la motivation
ou d’idée. Garder en tête qu’il s’agit d’un choix arbitraire pour m’aider à
avoir un rythme régulier, plus qu’une règle absolue.

Cette semaine toutefois je vais y annoncer quelque chose #teasing. Je suis
curieux de voir l’accueil qui y sera donné.

---

Je suis en train d’améliorer mon système de monitoring. Je n’ai toujours pas
trouvé la solution idéale, mais je vais partir sur [Netdata](https://github.com/netdata/netdata).
Il y a genre beaucoup trop de graphiques, et il semble assez compliqué d’avoir
les dashboards de tous les serveurs à un seul endroit. Ce qui m’a décidé c’est
qu’il est très simple à mettre en place et nécessite peu de configuration (il y
a déjà des alertes pré-configurées pour démarrer).

---

On va bosser ensemble avec [Maïtané](http://www.maiwann.net/) sur un outil pour
ergonomes. Je suis content quand mes compétences en développement logiciel
peuvent être vraiment et concrètement utiles à d’autres personnes et surtout
d’autres milieux. Ça me change de mes expériences passées.

---

[David](https://twitter.com/DavidBruant) m’a proposé que l’on s’appelle pour
discuter [d’un article](https://dtc-innovation.org/writings/2017/une-association)
qui m’a pas mal marqué à l’époque où je l’ai lu et qui a été l’un des
déclencheurs qui fait que j’en suis là aujourd’hui. La discussion a dérivé sur
le lien de subordination au sein des entreprises.

J’en ai rediscuté hier avec une amie, et je me rends compte que je ressors de
ces deux discussions avec plus de questions qu’à l’origine. Je laisse tout ça
reposer.

Ce qui me marque le plus au final c’est la façon dont les sujets qui
m’intéressent s’entrecroisent de discussion en discussion, de hasard en hasard.
Ce n’est pas la première fois que j’observe ça et ça me fait très plaisir de
sentir que « ça bouge à l’intérieur ».

---

J’ai légèrement avancé sur la prochaine version de [Lessy](https://lessy.yuzu.ovh).
Je suis en train de créer un système de « routines » pour les tâches qui
doivent se répéter. Tellement de questions se posent pour avoir une
fonctionnalité à la fois simple et flexible que j’ai bloqué une bonne heure
hier soir sur un écran. J’ai décidé de repartir de mes besoins immédiats, sans
en faire plus : meilleure décision.

---

Mes pensées vagabondent en ce moment du côté de la notion de « low-tech » à laquelle
j’accroche de moins en moins. Un autre terme me venait en tête : « small-tech »,
qui m’irait mieux. Je me demandais toutefois d’où me venait le terme. En
cherchant rapidement je vois que Laura Kalbag et Aral Balkan ont lancé [le site
small-tech.org](https://small-tech.org/), donc je présume que le terme m’est
venu par leur biais. Leur définition englobe toutefois des choses auxquelles je
n’avais pas pensé et je n’arrive pas encore à savoir si je suis aligné dessus.
Je vais encore laisser mon cerveau fouiller quelque temps tout seul une
définition qui me satisfasse.
