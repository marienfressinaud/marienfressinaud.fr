---
title: Sortie de Boop! 0.4 à l’arrache
date: 2019-11-16 20:15
---

Je me rends compte ce soir que cela fait un an que j’ai commencé mon générateur
de sites statiques, [<em lang="en">Boop!</em>](https://framagit.org/marienfressinaud/boop).
Comme je n’ai pas sorti de version depuis un petit moment, je fais donc ça
maintenant, au pied levé.

Pour les personnes qui débarquent, il s’agit d’un projet totalement personnel,
sans ambition aucune : je fais ça pour m’amuser.

Pour les personnes qui se posent la question : oui je continue de l’utiliser,
et même de plus en plus. C’est notamment <em lang="en">Boop!</em> qui est
utilisé pour mon blog « [carnet de flus](https://flus.fr/carnet/) » ainsi que
pour le site indiquant [le statut de Flus](https://status.flus.io/). À chaque
fois, cela m’a permis d’améliorer un peu plus le programme. Petit tour
d’horizon des nouveautés.

Le truc qui aurait dû être dans la dernière version – je sais pas pourquoi je
n’ai pas sorti une autre version d’ailleurs à l’époque – ce sont plus de
variables accessibles dans les <em lang="en">templates</em>. J’avais notamment
besoin d’afficher les séries sur la page du blog.

Tant qu’on est dans les <em lang="en">templates</em>, j’ai ajouté un nouveau
mot clé (`set`) permettant d’assigner des variables. [6 lignes de codes en plus](https://framagit.org/marienfressinaud/boop/commit/74706dc789a3879a6d4c7d8b08ed4be4700d4883),
c’est pas cher payé pour la prise de tête que [je me suis ainsi
évité](https://github.com/flusio/status/blob/master/templates/blog.html#L39-L41).

J’ai également ajouté le support des articles privés (qui n’apparaissent ni dans
les flux Atom, ni sur la page principale du blog). C’était fait [en 5
lignes](https://framagit.org/marienfressinaud/boop/commit/5e73a2c1c037bd899216ed7250f675389c9bc13d),
je vois pas pourquoi je me serais privé (_\*ba-dum tss\*_ 🥁).

Ensuite, la possibilité de modifier le « _slug_ » de la page principale du blog
permet notamment d’en faire une page d’accueil, [comme sur « carnet de
flus »](https://github.com/flusio/carnet/blob/master/configuration.yml#L6).
Cette fois-ci j’ai étonnamment [enlevé plus de lignes de code](https://framagit.org/marienfressinaud/boop/commit/e4612f49fc6889f7c20c0938911185da0fbe7eb6)
que j’en ai ajoutées, ça fait plaisir.

J’ai activé l’extension Markdown qui permet d’ajouter [la coloration
syntaxique](https://framagit.org/marienfressinaud/boop/commit/5c5e783fbe27e10ced71bea2bb8db7b6324503a6))
dans les articles. J’en avais particulièrement besoin pour ma série
« [Ergogames, une histoire de pizzas](serie/ergogames.html) ». Ça aurait été
illisible sans ça.

Un truc qui m’aura pris un peu plus de temps ([2h pour 50 lignes de code](https://tutut.delire.party/@marien/103092297547337615))
mais me permet de rester serein, c’est l’ajout d’[un système de cache](https://framagit.org/marienfressinaud/boop/commit/f1891cd5c955cc723d1b1211cfb71ca92e70b685)
pour les articles. Le site est désormais généré en 0,3 seconde au lieu de 2
secondes avant. 2s ça peut paraître peu, mais c’est énorme lorsque je relis un
article et que je le régénère plusieurs fois par minute.

J’ajoute à cela des micro-trucs, comme :

- le site qui est généré dans le répertoire `./_site` au lieu de `./site` (ça
  me permet de le distinguer très facilement des autres répertoires) ;
- le fait de pouvoir forcer la génération d’un flux RSS même lorsqu’il n’y a
  pas encore d’article publié ;
- l’affichage du temps de génération pour se rendre compte de si c’est trop
  long ou pas ;
- ou encore le fait d’afficher les derniers articles écrits en toute fin de
  terminal (ce qui m’évite de devoir scroller vers le haut pour retrouver mon
  article en cours d’écriture au milieu de la liste).

Je parle de « micro-trucs » mais ce n’est vraiment pas des détails pour moi :
c’est ce qui fait que je trouve mon expérience utilisateur bien au-dessus de
celles que j’ai pu avoir avec d’autres générateurs de sites statiques. Par
contre je parle bien de « mon » expérience : c’est totalement pensé par et pour
moi. Déjà il va falloir que j’améliore grandement la documentation pour en
faire un truc plus accessible à d’autres.

Tout ça pour dire que <em lang="en">Boop!</em> 0.4 est sortie, youpi ! 🎉
