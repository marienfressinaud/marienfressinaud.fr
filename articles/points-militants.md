---
title: Mes points médians sont-ils militants ?
date: 2021-04-28 09:10
---

Il y a quelques mois, le journal Marianne a publié [une tribune](https://www.marianne.net/agora/tribunes-libres/une-ecriture-excluante-qui-s-impose-par-la-propagande-32-linguistes-listent-les)
de 32 linguistes critiquant l’écriture inclusive. Cette tribune s’ouvre ainsi
(la mise en gras est de moi) :

> Présentée par ses promoteurs comme un progrès social, l’écriture inclusive
> n’a paradoxalement guère été abordée sur le plan scientifique, la
> linguistique se tenant en retrait des débats médiatiques. Derrière le souci
> d’une représentation équitable des femmes et des hommes dans le discours,
> l’inclusivisme désire cependant **imposer des pratiques relevant d’un
> militantisme ostentatoire** sans autre effet social que de produire des
> clivages inédits. Rappelons une évidence : la langue est à tout le monde.

La critique du militantisme est fréquente pour les « anti-inclusifs ». Elle m’a
questionné sur mon propre usage du point médian, la forme d’écriture inclusive
la plus critiquée.

J’ai discrètement utilisé ce point pour la première fois en [2017](2017-10-31-lessy-antlia.html),
sur ce blog. J’ai ensuite étendu cet usage à d’autres formes écrites
(courriels, réseaux sociaux, etc.) sans en parler, et sans qu’on m’en parle. Au
final, c’est à travers les critiques portées en direction de [Framasoft](https://framasoft.org)
que j’ai pris conscience de la sensibilité du sujet ([lire le texte en réponse
à ces critiques](https://framablog.org/2018/08/13/ecriture-du-blog-nous-ne-transigerons-pas-sur-les-libertes/)).

**Soyons clairs : le fait d’utiliser le point médian n’a jamais constitué une
volonté de ma part de l’« imposer » au reste de la société.** Qui fait donc
cela ? C’est un faux débat. Pour autant, inutile de le nier, il est
effectivement le marqueur d’un certain militantisme : celui qui revendique plus
d’égalité ainsi que la reconnaissance et la visibilité de toutes et tous au
sein de la société. Militantisme ostentatoire ? Vous jugerez.

**J’ai toujours considéré le point médian comme un outil à ma disposition.** Je
ne me souviens pas des raisons exactes qui m’ont poussé à l’utiliser la
première fois, mais aujourd’hui il me sert à m’adresser à un certain public. Il
me sert à dire que je suis sensible à la représentation des personnes, peu
importe le genre auquel elles s’identifient (ou non). Il me sert à m’exprimer
avec plus de précision. Il me sert à faire preuve d’empathie.

**Comme tout outil, il est également imparfait et pose certainement des
problèmes d’accessibilité.** Je suis sensible à cet argument et c’est pour cela
que j’en fais un usage raisonné :

- je privilégie d’autres formes autant que possible : formes épicènes, accord
  de proximité, jusqu’à expérimenter quelques néologismes comme « utilisateurice » ;
- je ne l’utilise pas systématiquement : par exemple, plus haut, lorsque j’ai
  écrit « anti-inclusifs » par respect du choix des personnes visées à
  privilégier le masculin générique ;
- je privilégie une forme légère : « utilisateur‧ices » au lieu de « utilisateur‧ice‧s » ;
- et qui facilite la prononciation : « inclusi‧ve » au lieu de « inclusif‧ve » ;
- pour les puristes, j’utilise d’ailleurs le point d’hyphénation au lieu du
  point médian, car [il serait ignoré par certaines synthèses vocales](https://matti-sg-fr.medium.com/point-m%C3%A9dian-final-point-dhyph%C3%A9nation-3f749c32b659).

J’ajoute que la question de l’accessibilité est bien trop sérieuse pour être
brandie comme un vulgaire « argument-chiffon ». Cela nécessiterait néanmoins un
article à part entière.

**Pour en revenir à la question des « militants du point médian », je n’en
comprends ni le fondement, ni la pertinence.** Je n’ai jamais croisé personne
affirmant qu’il fallait que cette forme devienne la norme. Je suis bien sûr
biaisé, peut-être avez-vous eu une expérience différente. Celle-ci vous
suffit-elle à généraliser ? La tribune aide justement à répondre à cette
question : « l’écriture inclusive n’a paradoxalement guère été abordée sur le
plan scientifique ». Bon courage pour prouver quoi que ce soit.

Pour ma part, je ne crois ni ne souhaite que le point médian se généralise.
Paradoxal ? **Je vois en fait le point médian comme un outil transitoire vers
quelque chose de plus accessible.** Peut-être un marqueur plus lisible ?
Peut-être une nouvelle forme neutre distincte du masculin ? Ou peut-être encore
la disparition du genre au sein la langue, qui sait ?

Pour conclure cet article, je me contenterai de citer une fois de plus la
tribune : « **la langue est à tout le monde** ».

---

J’effectue une veille active sur le sujet et [partage tout un tas de ressources
sur Flus](https://app.flus.fr/collections/1678881149731043972).
Si vous ne savez pas par où commencer et que vous souhaitez aborder le sujet
sérieusement, je vous conseille l’épisode « [Écriture inclusive : pourquoi tant
de haine ?](https://www.binge.audio/podcast/parler-comme-jamais/ecriture-inclusive-pourquoi-tant-de-haine) »
du podcast « Parler comme jamais » (il dure 46 minutes, vous êtes prévenu‧e !)
