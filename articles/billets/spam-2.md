---
title: Spam 2
date: 2022-03-07 12:55
---

Je découvre une nouvelle forme de spam :

1. le spammeur s’inscrit sur un site random avec mon adresse courriel ;
2. il met son message de spam avec un lien vers son site dans le champ nom d’utilisateur (ou autre) ;
3. je reçois le courriel de confirmation de compte, incluant un récapitulatif des infos, donc le message de spam.

J’imagine que ce mécanisme permet de passer à travers un certain nombre d’antispams puisque l’envoyeur du courriel est légitime (bon, en l’occurrence le serveur d’envoi semble mal configuré et le message a quand même été marqué comme spam).

Finalement, ce qui me saoule le plus c’est que je me trouve inscrit de force sur un site allemand « spécialiste de la fourniture d’embrayages, de freins et de disques » dont je n’ai que faire.