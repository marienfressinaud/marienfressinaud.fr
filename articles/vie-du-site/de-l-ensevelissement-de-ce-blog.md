---
title: De l’ensevelissement de ce blog
date: 2023-11-16 17:55
---

Depuis que je suis petit, j’ai toujours aimé écrire.

J’ai retrouvé récemment des écrits dans de vieux cahiers (CE2 et CM1) dont j’avais été particulièrement fier à l’époque.
Au collège, je m’amusais à écrire de courtes histoires qui mettaient en scène mes pions de l’époque et que je distribuais ensuite à mes copains d’internat et de classe.
Le jour où ma prof de français est tombée dessus, j’avoue avoir ressenti un mélange de honte et de fierté (elle m’a encouragé, merci à elle !)
Au lycée, j’ai débuté un blog sur lequel je publiais aussi bien des billets d’humeur, que des poésies ou encore des tutos HTML et CSS.

Ce blog-ci existe depuis au moins 2011.
J’y ai parlé de mes premières tentatives de projets Web, tout comme de mes projets plus aboutis, comme FreshRSS.
Je n’ai jamais été extrêmement prolifique, mais j’ai toujours espéré trouver l’énergie de m’y mettre « vraiment ».
En fait, c’était l’envie qu’il me manquait surtout.

La « ligne éditoriale » ne m’a jamais trop plu : j’aurais plutôt eu envie de publier de courts textes, peut-être plus expérimentaux.
J’aurais pu la faire évoluer, mais je n’étais pas à l’aise avec ça sur mon site personnel.
De plus, j’ai toujours eu du mal à voir attaché mon nom aux articles.
Je suis bien plus à l’aise sous une forme d’anonymat.

Il y a quelques jours, j’ai donc décidé de supprimer la page principale du blog (celle qui listait les articles).
Mon site se réduit donc à la page d’accueil pour me présenter et une page expliquant comment me contacter.
Je suis plus à l’aise avec ça.

En revanche, pour l’instant, j’ai décidé de conserver les articles et le flux Web.
Les personnes qui suivent ce blog depuis un agrégateur pourront donc continuer de le faire.
En effet, je ne me ferme pas non plus la porte à l’écriture d’autres articles à l’avenir.
Seulement, je n’ai aujourd’hui plus aucune pression pour le faire, et c’est bien mieux !

Quant à mes envies d’écrire, je compte éventuellement ouvrir un blog ailleurs.
Mais ça, je n’en parlerai pas !
