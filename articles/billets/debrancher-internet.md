---
title: Technique de décrochage : débrancher Internet
date: 2022-02-08 20:25
---

En ayant [quitté Twitter](quitter-twitter.html) le mois dernier, j’avais déjà bien diminué les distractions numériques. Il en subsistait toutefois encore : le tchat avec les copain‧es, Mastodon, les courriels qui arrivent, etc. Je n’allais quand même pas tout couper ! Quoique…

**Il y a environ un mois, j’ai commencé à m’accorder des sessions de travail hors-ligne.** C’est-à-dire que, selon les cas, je coupe la connexion dans les paramètres réseaux, voire je débranche littéralement mon câble <abbr>RJ45</abbr>. C’est un peu radical, je vous l’accorde, mais l’expérience est intéressante pour le changement opéré dans ma manière d’aborder le numérique.

Le premier impact, celui que je recherchais, est que je ne vérifie plus frénétiquement si j’ai de nouvelles choses à lire sur Mattermost ou Mastodon. C’est bête, mais c’est très efficace. Comme je ne suis plus happé par ces plateformes, **mon cerveau se tourne plus facilement vers de l’écriture, ou vers des améliorations pour le site.**

Ensuite, il faut s’adapter. J’écoute beaucoup de musique via des plateformes de <i lang="en">streaming</i> quand je travaille. **Désormais, je redécouvre la musique en local et j’ai commencé à utiliser [Lollypop](https://wiki.gnome.org/Apps/Lollypop) depuis peu[^1].** Heureusement, ma musicothèque est relativement fournie. Lollypop, via sa fonction de suggestions, me permet de redécouvrir des titres que je n’ai pas écoutés depuis longtemps ; c’est plutôt chouette.

[^1]: Après de longues années de bons et loyaux services de la part de [Rhythmbox](https://wiki.gnome.org/Apps/Rhythmbox), beaucoup trop bugué ces derniers temps. Je ne regrette pas à vrai dire : je trouve Lollypop bien plus agréable à utiliser au quotidien.

Côté pro, c’est un peu plus embêtant, mais on s’en sort. **J’utilise [DevDocs](https://devdocs.io/) pour avoir de la documentation technique hors-ligne.** Ça ne permet pas de répondre à tout, mais c’est souvent suffisant. Je me dis parfois qu’il manque l’équivalent pour [StackOverflow](https://stackoverflow.com/), mais ça me paraît plus compliqué à mettre en place. D’un côté, ça permet de passer plus de temps à se creuser les méninges et c’est peut-être pas plus mal.

D’autre part, comme [Flus](https://flus.fr) est avant tout un outil qui se connecte à d’autres sites, il a fallu ruser. **J’ai créé [un petit serveur de « mock »](https://github.com/flusio/flusio/blob/6d7776c5fdd4446b3d82251bfc4cf1219721fdc4/tests/mock_server.php) auquel je peux indiquer « pour telle requête <abbr>HTTP</abbr>, renvoie telle réponse ».** Je l’utilise avant tout pour les tests — qui passent désormais (quasiment) tous en hors-ligne —, mais il peut aussi fonctionner en phase de développement lorsque j’en ai besoin. C’était moins compliqué à faire que ce que je pensais.

Ça ne fait qu’un mois que j’ai commencé à travailler ainsi. Je ne prétends pas que c’est la panacée ; c’est une manière de faire évidemment très limitée. **En revanche, l’effet est globalement positif sur ma capacité de concentration et sur ce que je suis en mesure de produire.** Expérience en cours donc.