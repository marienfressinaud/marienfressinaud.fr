Date: 2012-02-12 23:12
Title: Sortie de MINZ 0.5
Slug: sortie-de-minz-0-5
Tags: MINZ, php
Summary: MINZ (ou Minz) est un framework PHP que j’ai développé de 2011 à 2013. C’est probablement le projet qui m’a le plus fait progresser dans mes méthodes de développement bien que le projet reste imparfait par bien des aspects. Le code est toujours disponible [sur GitHub](https://github.com/marienfressinaud/MINZ).

Après la mise à jour du site hier, j’ai décidé de mettre à jour mon framework PHP, MINZ.

Au programme, pas mal de grosses nouveautés plus ou moins utiles : gestion de l’internationalisation, amélioration de la gestion des sessions, ajout d’un historique de navigation, des classes toutes faites pour gérer les utilisateurs, arrivée des exceptions et de nombreuses corrections de bugs.

L’application de tests a été revue au passage pour montrer les nouveautés. La connexion se fait à travers le système OpenID que je prends en charge nativement dans MINZ (en plus du protocole XMPP). Ces deux systèmes font leur arrivée par la petite porte ; à comprendre que MINZ ne prend en charge que la phase de connexion. De plus, pour ce qui est de XMPP il y a un problème lorsque je l’utilise sur ce serveur et je suppose que c’est le port 5222 qui est bloqué, ce qui m’empêche de l’utiliser.

Pour ce qui est des exceptions, je n’ai pas encore trop poussé le concept et il y a encore un gros boulot à faire de ce côté là. Mais je pense que dans le futur ce sera bien plus efficace et très utile pour le coeur du framework.

Un petit mot sur l'internationalisation : la classe `Translate` me permet à partir d'une "clé" d’aller piocher dans des fichiers de traduction la valeur correspondante à la langue désirée. Je ne l’utilise pas sur le site, mais l’application de test permet de montrer le principe.

Un autre petit plus à l’application de tests, c’est l’affichage des logs de l’application.

Pour finir dans les choses importantes, l’historique de navigation permet de créer un lien pointant vers une page déjà visitée précédemment. Je l’utilise sur le site pour mes liens de retour. Cela permet de ne pas perdre la navigation et ça fonctionne à peu près comme l’historique du navigateur (même si des problèmes techniques m’empêchent de pousser le concept jusqu’au bout). Par exemple, si vous cliquez sur le lien pour accéder à la page de présentation de MINZ, le lien "retour" présent sur la page vous permettra de revenir à la page où vous êtes actuellement ☺

Ce qui est prévu pour la suite : amélioration de la structure du framework avec l’utilisation poussée des exceptions PHP, création d’une classe permettant de gérer facilement la mise en cache, création de nouveaux `Models`, de la documentation (/!\) et bien sûr, l’habituelle correction de bugs ☺

À bientôt j’espère pour une version 0.6 !
