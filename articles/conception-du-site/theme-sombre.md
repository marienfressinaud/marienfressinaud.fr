---
title: Thème sombre
date: 2022-02-05 22:36
---

Je viens d’ajouter un thème sombre à ce site. Par défaut, il s’affichera en suivant les préférences de votre système, mais vous pouvez forcer le thème à partir du sélecteur qui se trouve dans le pied de page du site.

La peinture est fraîche, il est possible que des soucis d’affichage subsistent. N’hésitez pas à me faire signe si vous vous en apercevez !

C’était l’occasion pour moi de découvrir comment m’y prendre. Je ne suis pas encore satisfait de ma technique, donc j’aurai l’occasion de creuser encore.