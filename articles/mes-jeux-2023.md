---
title: Mes jeux 2023
date: 2023-12-24 10:00
---

Début 2023, je me suis fait une liste de jeux à jouer durant l’année.
Je suis heureux d’avoir fait ça, car ça m’a permis d’être moins dans la « consommation » et de mieux apprécier mes découvertes.
Petite mise en avant des jeux qui m’ont le plus marqué.

## Hollow Knight

Dans [Hollow Knight](https://www.hollowknight.com/), on incarne un chevalier insecte muni de son aiguillon.
On ne nous dit pas grand-chose, et c’est à nous de comprendre l’intrigue.
On y explore une immense carte en deux dimensions, tout en combattant tout un tas d’ennemis.
Les boss sont particulièrement velus.

C’était l’un de mes défis de l’année car le jeu est connu pour sa difficulté.
De fait, j’ai eu du mal à me plonger dans le jeu, et il s’en est fallu de peu pour que je le laisse trainer dans un coin.
Au final, je me suis remotivé et j’ai fini par me prendre au jeu.
Et une fois le jeu fini une première fois… j’ai immédiatement lancé une seconde partie.
Je voulais jauger ma progression, et je n’ai pas été déçu : le sentiment de facilité sur les premiers niveaux est grisant.
J’ai rarement aussi bien ressenti le fait d’être devenu meilleur à un jeu.

## Zelda Tears Of The Kingdom

Comme dans tout bon Zelda qui se respecte, on incarne Link qui doit sauver la Princesse Zelda, et tout le royaume d’Hyrule tant qu’à faire.
J’avais adoré le précédent opus (Breath of the Wild), donc je ne pouvais pas passer à côté de celui-ci.

La particularité de [Tears of the Kingdom](https://zelda.nintendo.com/tears-of-the-kingdom/fr/) réside dans la possibilité de fabriquer des outils.
J’avoue avoir relativement peu joué de cette mécanique, sauf à recréer un peu toujours les mêmes objets (notamment le scooter volant, très pratique dans les sous-terrains).

J’ai trouvé le jeu un peu moins maîtrisé sur certains aspects comme les sanctuaires ou les pouvoirs.
En revanche, la mise en scène et l’ambiance sont folles, et j’ai savouré chaque minute sur ce jeu.
Mention spéciale à l’arrivée dans le Temple du Vent, incroyable.

## Outer Wilds

[Outer Wilds](https://www.mobiusdigitalgames.com/outer-wilds.html) est une magnifique aventure d’exploration spatiale.
On est balancé aux manettes d’une navette et muni d’un traducteur capable de déchiffrer d’étranges symboles laissés par une civilisation disparue il y a longtemps.
À partir de là, on va devoir résoudre un puzzle à l’échelle de l’univers, en explorant chaque planète de notre système solaire une à une.

La puissance du jeu réside dans la découverte des mécaniques du jeu.
Tout est sous nos yeux dès le début du jeu, et on pourrait résoudre le jeu en moins d’une heure (avec beaucoup de chance toutefois).
Il en faut toutefois une bonne vingtaine pour réussir à recoller tous les morceaux.

Bon, clairement mon coup de cœur de l’année <3

Je guettais ce jeu depuis plusieurs années.
Il était annoncé sur Switch, mais a été repoussé sans qu’une nouvelle date ne soit donnée.
On me l’a finalement offert sur Steam.
J’ai donc pu y jouer sur PC… juste avant que le jeu ne sorte sur Switch cette fin d’année !

En revanche, une fois le jeu terminé, pas moyen d’y rejouer puisqu’on connait désormais les mécaniques.
Ce qui m’amène à ce conseil : ne vous spoilez pas le jeu !
Ne regardez pas la bande-annonce, ne recherchez pas d’informations en ligne, et suivez simplement le conseil de tous les joueurs et joueuses qui sont passés avant vous : ne vous spoilez pas (oui je me répète).

Et si vous aimez le jeu, n’hésitez pas à jouer à son DLC, Echoes of the Eye (dans une ambiance plus angoissante, vous êtes prévenu⋅es !)

## Celeste

[Celeste](https://www.celestegame.com/) est un jeu de plateforme en deux dimensions particulièrement exigeant.
Il raconte l’histoire d’une jeune fille qui veut gravir une montagne.
C’est finalement sa dépression et son anxiété qu’on finit par affronter au fil de l’ascension.

C’est le dernier jeu auquel j’ai joué cette année et c’était mon deuxième défi de l’année.
En effet, je ne joue pas trop aux jeux de plateforme, mais Hollow Knight m’avait déjà redonné un peu goût au genre.

Mon avis est (un peu) mitigé vis-à-vis de ce jeu.
Je pense que j’en attendais beaucoup, mais que le fait d’avoir atteint la « première » fin du jeu relativement rapidement ne m’a pas laissé le temps de l’apprécier à sa juste valeur.
Pour autant, j’ai beaucoup aimé y jouer.
J’ai de toute façon encore beaucoup de choses à faire, notamment les « faces B » des chapitres (des versions beaucoup plus ardues des cartes).

Je continue donc d’y jouer et, comme Hollow Knight, le jeu sait faire progresser et nous en faire prendre conscience.
Je pense que je l’apprécierai de plus en plus avec le temps.

## Quelques jeux en rab’

J’ai joué à plusieurs autres jeux en plus de ceux listés ci-dessus, je les balance ici en vrac :

- [Powerwash Simulator](https://powerwash-simulator.square-enix-games.com/fr/), un simulateur de lavage à haute-pression, c’est très de niche, mais il procure une satisfaction énorme à laver des surfaces encrassées.
- [Brotato](https://thomasgervraud.com/press/brotato/), un jeu de survie (à la Vampire Survivors pour celleux qui connaissent) où l’on équipe notre patate de divers armes pour survivre à des vagues d’ennemis ; c’est un gameplay très bête et méchant, mais ça peut détendre.
- [Superflu Riteurnz](https://studios.ptilouk.net/superflu-riteurnz/), le jeu d’enquête développé par l’ami Gee, on y incarne Harpagon Lonion (aka Superflu), assisté de Sophie, pour débusquer un mystérieux voleur de pommes ; c’est idiot, c’est fun, c’est coloré, c’est très chouette, jouez-y !
- [F-Zero 99](https://www.nintendo.com/jp/switch/baypa/index.html), jeu de courses de voitures à l’ancienne, mais sauce battle royale ; chouette de jouer contre 98 adversaires, et assez exigeant, mais un peu répétitif à la longue (il y a peu de cartes).
- [Minetest](https://www.minetest.net/), un clone de Minecraft, on mine des cubes, on construit des bâtiments, et on occit des monstres ; bref, la routine.

## Conclusion

Je recommande chacun des 4 premiers jeux cités sans hésitation (aux amateurs de ces genres de jeux en tout cas).
Et s’il ne devait y en avoir qu’un, ce serait Outer Wilds sans hésiter !
Je considère que cette année 2023 a été celle où je me suis le plus amusé d’un point de vue vidéoludique.

Je suis particulièrement heureux de m’être fait cette liste de jeux en début d’année.
Ça m’a laissé le temps de m’impatienter pour chacun d’eux, un peu comme un cadeau de Noël qu’on attend de plus en plus à l’approche du 25 décembre.
J’ai aussi davantage apprécié chacun de ces jeux en faisant en sorte que ce ne soit pas des jeux achetés sur un coup de tête.
D’ailleurs, j’ai été beaucoup plus « consommateur » des jeux qui sortaient de cette liste ; j’ai moins pris le temps de les apprécier.

En 2024, je compte réitérer l’expérience de manière élargie : je compte me faire une liste de livres, musiques, films, et bien sûr jeux vidéos à acheter.
