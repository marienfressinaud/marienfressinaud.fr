---
title: Questions d’entretien
date: 2022-05-11 10:23
---

J’ai passé un entretien il y a quelques jours. Je liste ci-dessous quelques questions que j’ai posées :

- quel est le modèle économique de l’entreprise ?
- par qui / comment les clients sont-ils choisis et démarchés ?
- arrive-t-il de travailler avec des clients parce que l’entreprise n’a pas le choix de faire autrement ?
- combien de personnes travaillent dans l’entreprise ?
- quelle est l’ancienneté moyenne des salarié·es ?
- quels sont les métiers exercés dans l’entreprise ?
- comment les personnes communiquent-elles entre elles ?
- vais-je travailler en équipe ?
- un mi-temps est-il possible ?
- combien serai-je payé ?
- de quelle convention collective dépend l’entreprise ?
- quels sont les avantages apportés par l’entreprise ?
- le télétravail est-il possible ?
- y a-t-il une flexibilité sur les horaires de travail ?
- comment sont organisés les entretiens annuels ?

Par ailleurs, on m’a parlé de [societe.ninja](https://www.societe.ninja/) qui permet de retrouver toutes les informations publiques des entreprises, incluant notamment les comptes et les actes officiels. Ça peut aider à préparer son entretien. L’outil derrière est [libre](https://git.cybertron.fr/lionel.vest/societe.ninja), sous licence <abbr>AGPL</abbr>.