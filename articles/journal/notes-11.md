---
title: Notes #11
date: 2020-07-13 10:00
blog: false
---

Certaines choses sont amusantes quand même : il m’aura fallu annoncer dans [ma
dixième « Notes »](notes-10.html) que je voyais désormais un peu mieux à quoi
me servaient celles-ci pour que j’arrête d’en écrire pendant 2 mois. Bon, à
vrai dire elles ont plutôt migré vers [le carnet de flus](https://flus.fr/carnet/),
mais je n’y ai abordé que des aspects liés à [Flus](https://flus.fr). J’ai en
effet réalisé que ces éléments avaient toute leur place là-bas. Finalement
c’est ce qui aura signé l’arrêt des notes ici.

Cette note n’est pas pour annoncer que je vais les reprendre ; après tout,
j’ai eu du mal à garder mon rythme de dessin à la fin du confinement et je ne
ferai (presque) pas de pain cet été, la température grenobloise ne nécessitant
pas d’être plus réchauffée. J’aurais bien éventuellement des choses à raconter
liées à mes engagements chez [Framasoft](https://framasoft.org) et [Sud Web](https://sudweb.fr),
mais enfin pour l’instant je cherche d’abord un rythme hebdomadaire de
contributions.

Une autre chose qui me gênait avec ces notes, c’est qu’elles ne sont pas
publiées sur [la page d’accueil du blog](blog.html) (je ne compte pas changer
cela non plus), ce qui laisse penser que je n’ai rien publié du tout depuis
mars. Je vais par contre tenter de m’accorder au moins une heure par semaine
pour rédiger des choses « autres » sur le blog (c’est pas les sujets qui me
manquent), en espérant reprendre mon rythme de 2019.

… Mais bon, tous les blogs ne se terminent-ils pas par une note annonçant une
reprise des publications ? 😇
