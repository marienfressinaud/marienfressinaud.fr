Date: 2017-06-05 21:10
Title: Project Zero 0.2
Slug: project-zero-0-2
Tags:
Summary: Project Zero sort en version 0.2 et ajoute un système de tâches… particulier :).

Après [une version 0.1](https://marienfressinaud.fr/project-zero-0-1.html)
sortie fin janvier, je sors aujourd’hui (avec seulement un mois de retard) la
version 0.2 de Project Zero, mon mini-système de gestion de projets et de
tâches.

Le code source se trouve [sur GitHub](https://github.com/marienfressinaud/project-zero),
sous licence MIT et vous pouvez toujours l’utiliser sur [zero.marienfressinaud.fr](https://zero.marienfressinaud.fr).
L’adresse n’a pas (encore) changé mais passe désormais en HTTPS.

## Les nouveautés de la 0.2

Cette nouvelle version apporte la gestion de listes de tâches (ou « todo
lists »), fonctionnalité dont j’avais le plus besoin. J’utilisais auparavant
[Todoist](https://todoist.com) qui me convenait plutôt bien, mais je souhaitais
un système plus proche de mon mode de fonctionnement. Qu’apporte vraiment
Project Zero par rapport à n’importe quel autre gestionnaire de tâches ?

Tout d’abord, l’ajout des tâches a été pensé pour se faire le jour même. La
plupart du temps, en arrivant le matin au boulot, je me prépare une liste des
choses que j’aimerais régler pendant la journée. Quel meilleur moment pour
savoir ce que l’on souhaite faire que le matin ?

Bien sûr, il m’arrive aussi souvent d’avoir des idées de choses à faire que je
ne réaliserai pas le jour même, pour cela une seconde liste appelée « backlog »
vient s’ajouter à la première dans laquelle je peux venir piocher pour préparer
ma liste de la journée.

Les tâches que je n’arrive pas à terminer sont marquées en attente et viennent
alimenter une troisième liste. En préparant ma liste de la journée, les tâches
non réalisées la veille me sont proposées à nouveau et je peux soit indiquer
que je l’ai en fait déjà terminée, soit la replanifier pour aujourd’hui. Si je
la replanifie, un indicateur est incrémenté. Cet indicateur me sert à me
prévenir que j’ai trop replanifié une tâche et me propose au bout de 2 fois de
découper la tâche ou de la renommer pour la rendre plus facile à terminer.

Un second indicateur particulièrement utile pour les tâches du « backlog »
indique le nombre de semaines depuis que la tâche a été créée. Au bout de 2
semaines, l’indicateur me propose d’abandonner la tâche vu qu’il y a de fortes
chances que je ne la termine jamais.

Ces deux indicateurs me permettent d’éviter de conserver des tâches pendant
trop longtemps. Cela me permet d’avoir un système de priorisation non basé sur
une appréciation subjective mais basé sur le temps et sur la répétition ; ainsi
je peux réordonner les tâches au fil du temps.

![Dashboard de Project Zero 0.2](images/projectzero/2017-06-05-0.2-dashboard.png)

## Les dates d’échéance ?

J’ai décidé de ne pas gérer les dates d’échéance. C’est encore une réflexion en
cours car j’avais vraiment dans l’idée de les gérer mais j’ai finalement conclu
à la non nécessité de la fonctionnalité. Une date d’échéance peut avoir deux
significations :

- une date limite permettant d’éviter que la tâche reste indéfiniment dans la
  liste, rôle déjà porté par les indicateurs ;
- une date de réalisation et, dans ce cas, un calendrier fait très bien
  l’affaire.

De plus, j’avais peur de la complexité car cela m’aurait mené à devoir
(vouloir ?) gérer la répétition des tâches dans le temps et la saisie intuitive
des dates (taper « demain » pour saisir la date du lendemain par exemple).

## En prévoyant la suite…

Cette nouvelle version apporte un gros morceau qui fait que Project Zero me
devient de plus en plus utile. Pour autant, je n’ai pas encore vraiment le
réflexe de m’y connecter et je n’y ai d’ailleurs pas du tout touché pendant le
mois d’avril (ce qui explique le retard pris pour cette sortie). J’ai tout de
même ajouté pas mal de choses en mai qui me facilite l’usage (ordonnancement,
édition, abandon et indicateurs).

La version à venir (la 0.3 donc) me servira surtout à consolider ce que j’ai
fait jusqu’à maintenant et devrait ajouter des mécanismes poussant à la
« fidélisation » (meilleure gestion des petits écrans, emails de rappel, joli
logo, amélioration de l’ergonomie et refonte du design, éventuellement
ludification). Je n’ai aucune idée de la date de la prochaine sortie car j’ai
d’abord besoin d’identifier ce qui bloque mon usage.

J’ai pas mal d’idées de ce que pourrait devenir Project Zero et des
fonctionnalités qui pourraient m’être utiles, mais je souhaite vraiment monter
ce projet petite brique par petite brique en m’assurant que le tout est
utilisable et utilisé.
