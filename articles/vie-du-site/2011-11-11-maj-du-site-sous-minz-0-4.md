Date: 2011-11-11 20:43
Title: Màj du site sous MINZ 0.4
Slug: maj-du-site-sous-minz-0-4
Tags: màj, MINZ
Summary: Mon site existe depuis 2010. Lors de la migration d’avril 2016, j’ai fait le grand ménage dans les articles. Je trouvais toutefois intéressant de conserver les articles liés à la vie du site pour voir comment celui-ci a grandi.

Article rapide pour indiquer que j’ai passé mon site sous la dernière version de mon framework. Avec ça arrive ma nouvelle galerie photos développée par mes soins.
Notez que tout ce qui est url rewriting est géré par le framework, et ça c’est top ☺

Bref, la migration s’est bien passée ;)

EDIT. J’en ai profité pour mettre à jour l’application MINZ de tests, et je l’ai passé en version... 0.5 beta ! ☺ C’est donc la version encore en développement. Au programme notamment, l’internationalisation, qui a été plus facile à mettre en place que ce que je ne croyais. J’en ai aussi profité pour épurer encore plus le design de base ^^ ... oh :O je viens de découvrir une grosse faille en l’installant ☺
