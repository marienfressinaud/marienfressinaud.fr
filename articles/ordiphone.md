---
title: Ordiphone
date: 2021-10-04 21:38
---

Je discutais hier avec un copain de nos usages respectifs de nos téléphones dits _intelligents_. La discussion m’a permis de mieux mesurer le parcours que j’ai réalisé ces dernières années. De fait, j’ai totalement réussi à redonner à mon téléphone la place qui lui incombe : un simple outil à mon service. Je me disais que c’était l’occasion de partager quelques trucs et astuces que j’ai mises en place.

## Désinstaller

**La principale action que j’ai réalisée a aussi été la plus compliquée et la plus longue : désinstaller un maximum d’applications.** Ce fut compliqué car, pour chaque appli, on mesure facilement ce qu’elle nous apporte ; plus difficilement ce qu’elle nous impose. Surtout, on s’imagine difficilement vivre sans — on avait sûrement une bonne raison de l’installer en premier lieu ! De mémoire, voici les principales applications qui m’ont posé souci :

- Twitter — positif : certain d’avoir toujours quelque chose de nouveau à lire ; négatif : chronophage, peut facilement générer des émotions négatives ;
- Signal — positif : discussion facilitée avec les proches, on ne rate aucune info ; négatif : beaucoup trop de notifications, l’appli s’impose rapidement comme indispensable ;
- Firefox — positif : le Web dans la poche, tout est à notre portée ; négatif : **TOUT** est à notre portée ;
- K9 Mail (courriels) — positif : facilite encore un peu plus nos échanges, permet de transférer facilement des infos sur son téléphone via un simple courriel ; négatif : tendance à vérifier ses courriels dès qu’on a 2 minutes de libre ;
- Google Maps — positif : jamais perdu ; négatif : jamais perdu.

**Plutôt que de peser de manière rationnelle le pour et le contre de garder ou non chaque application, je crois les avoir toutes désinstallées sur un coup de tête** (« je teste quelques jours, je vois ce qu’il se passe »). Le plus compliqué a été de se convaincre que je n’avais pas besoin de mes courriels sur le téléphone. Je pense que ça fait au moins 2 ans que j’ai désinstallé K9 Mail et je ne regrette absolument pas 🙂. Pour passer à l’action, je me suis imposé quelques contraintes.

## Se contraindre

La première contrainte que je me suis imposée a été de limiter à deux les bureaux virtuels de l’écran d’accueil : un pour les applis, un autre pour mon agenda. **J’ai fini par rendre cette contrainte encore plus stricte : un seul écran afin d’afficher mon agenda.** Les applications visibles sont dans le <i lang="en">deck</i> permanent et sont limitées à 4 en plus de l’icône pour ouvrir la liste des applications. Mes applications visibles sont les suivantes :

- l’appareil photo ;
- une application de prise de notes ;
- l’horloge pour les alarmes ;
- l’appli de <abbr>SMS</abbr>.

**La seconde contrainte est très similaire : un seul écran pour lister mes applications.** Aujourd’hui, cet écran contient 24 applications. Je ne vous les liste pas toutes mais, pour les plus utiles, on retrouve les applications listées précédemment ainsi qu’un explorateur de fichiers, la galerie photo, un lecteur de musiques, l’appli de contacts, etc.

**Ma dernière contrainte forte a été de limiter au maximum les applications autorisées à générer des notifications.** Cette contrainte est très facile à s’imposer une fois qu’on a réduit la liste des applications, mais reste à mon avis importante à garder en tête. En l’occurrence, les seules applications qui me notifient sont les <abbr>SMS</abbr>, le téléphone et l’agenda.

## Accepter l’inconfort

À cette étape, vous vous dites que c’est n’importe quoi, que je dois aimer me faire mal — et alors ? —, que vous vous seriez incapable de limiter autant vos usages. Mais attendez, j’en rajoute :

- mon téléphone est un modèle de 2013, acheté en 2015 (<i lang="en">fun fact</i>: feu mon Firefox <abbr>OS</abbr> venait de griller sur une prise en Bulgarie, je cherchais le modèle moins cher possible du magasin pensant que j’en rachèterais un en rentrant en France… 6 ans plus tard, il est toujours vivant !) ;
- il tourne sous LineageOS 11 (Android 4.4, soit une version datant de 2014) ;
- lié à ça, rien n’est vraiment à jour, il est donc bourré de failles et c’est sans doute ce qui m’enquiquine le plus ;
- le navigateur de base — que j’ai conservé — est extrêmement lent et pas du tout à jour… je n’ai donc pas besoin de me forcer pour ne pas l’utiliser ! Il est juste suffisant pour me dépanner de temps à autre, mais je pense que je pourrais tout à fait m’en passer ;
- l’appareil photo est nul nul nul, je suis incapable de faire des photos pas floue, mais c’est artistique.

Je ne vais pas non plus affirmer que tout est parfait : oui je rate parfois des infos qui passent par Signal, oui Google Maps aurait pu me dépanner plus d’une fois, oui ça va sans doute devenir de plus en plus pénible au fur et à mesure qu’on nous impose l’usage des <i lang="en">smartphones</i> (coucou les banques 👋). **Mais, honnêtement ? je suis tellement plus serein vis-à-vis de mon téléphone depuis ces dernières années que je n’ai absolument aucune envie de revenir sur mes choix.**

## Un ordiphone ?

**Finalement, la question qui reste en suspens est celle de savoir si j’ai toujours besoin d’un téléphone intelligent dans la poche ? Je pense que je pourrais effectivement m’en passer.** Les seules choses dont j’ai l’impression d’avoir vraiment besoin sont le téléphone et les <abbr>SMS</abbr>. J’aurais du mal à me passer du réveil, de l’agenda, voire du lecteur de musiques, mais je suis sûr de trouver des moyens de contourner si cela venait à m’être imposé.

Plusieurs points positifs à tout ça :

- je n’ai plus l’impression de faire d’efforts au quotidien pour me détacher du téléphone, mes habitudes ont totalement changé. Vous imaginez pas le bien fou que ça fait :) ce n’est plus qu’un bout de plastique qui traîne dans un coin de mon appartement et qui fait du bruit occasionnellement ;
- le jour où j’aurais besoin de changer de téléphone, j’aurais l’embarras du choix tout en pouvant privilégier des critères de durabilité (l’actuel s’en sort bien vu le nombre de fois où il est tombé) ;
- ça me réaligne un peu avec mon idéal écolo. Je ne sauverai pas la planète avec cette démarche, mais le jour où le monde fera face à une pénurie d’électricité, le manque de téléphone ne sera au moins pas un problème pour moi 😜.

Mon prochain objectif : avoir une démarche similaire avec le <abbr>PC</abbr> qui me pose d’énormes problèmes de distractions. J’ai bien peur que ce soit plus compliqué !
