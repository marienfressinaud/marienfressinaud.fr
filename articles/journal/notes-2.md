---
title: Notes #2
date: 2020-02-03 15:00
blog: false
---

J’ai commencé à jouer avec [BookStack](https://www.bookstackapp.com/) la
semaine dernière pour documenter mon infrastructure. J’en suis tombé amoureux
quand j’ai réalisé qu’ils intégraient [draw.io](https://www.draw.io/) pour
réaliser des diagrammes directement depuis l’éditeur. De fait, j’ai pu
représenter la façon dont mon infrastructure est constituée, et j’ai même
débuté des maquettes pour Flus. C’est LE système de wiki que j’attendais.

Ça m’a quand même demandé un peu de temps pour l’installer car j’ai dû
mettre en place une base de données MySQL. J’en suis un peu triste, mais ça
valait le coût. J’ai également réalisé que l’extension GD de PHP (permettant
de manipuler les images) était mal installée sur mon serveur, empêchant
l’intégration de certaines images. Les messages d’erreur étaient tellement peu
clairs que ça m’a bouffé 2 heures à fouiller le code de BookStack. Le bon côté
étant que je réalise à quel point je suis à l’aise pour rentrer dans du code
qui n’est pas le mien.

---

J’ai refait du pain de la même manière que [la semaine dernière](notes-1.html),
mais en changeant de sel : il a encore correctement gonflé, mais il est
excellent cette fois-ci. Il faudra juste que j’arrête de partir en soirée en me
disant que je le finirai en rentrant.

---

Ce weekend c’était l’assemblée générale annuelle de [Framasoft](https://framasoft.org).
Je suis super heureux d’avoir revu les copain·es et épaté de voir que l’asso
fonctionne aussi bien que ce soit concernant les projets, les finances ou les
rapports humains. Vraiment l’impression de vivre quelque chose d’unique.
Content également de garder un rôle de coprésident : comme je ne vais pas
avoir beaucoup de temps à donner à l’asso cette année, ça me permet au moins de
participer en assumant une part de la responsabilité associative.

Je me rends par contre compte à quel point j’ai de plus en plus de mal à rester
concentré au sein d’un groupe bruyant (ou j’identifie de mieux en mieux ma
fatigue ?) Même en ayant passé une très bonne nuit, j’ai senti l’épuisement
arriver rapidement dimanche matin.

---

J’ai acheté une souris : une [Logitech MX Master 2S](https://www.logitech.fr/fr-fr/product/mx-master-2s-flow)
en remplacement de ma précédente [Sculpt Ergonomique de Microsoft](https://www.microsoft.com/fr-fr/p/souris-microsoft-sculpt-ergonomic/8vq1d8qsvt22?activetab=pivot%3aoverviewtab).
J’aurais souhaité me passer de cet achat, surtout que ma souris était vraiment
bien… sauf que le clic principal commençait à lâcher (un clic sur trois ne
faisait plus rien). Elle aura tenu moins de 3 ans environ ; autant dire que je
suis extrêmement déçu. J’aime moins la forme de la nouvelle et le scroll est
moins précis, mais je pense qu’il s’agit en partie de changer d’habitude.

J’envisage également l’achat d’un ordinateur fixe. Mon portable (Dell XPS 13 de
2015) montre des signes de faiblesse : la ventilation notamment a toujours été
catastrophique, mais ça ne va pas en s’arrangeant. Dans l’idée je le garderais
pour mes déplacements qui sont encore assez nombreux, mais de cette manière il
devrait pouvoir encore vivre quelques années. Forcément qui dit nouvel
ordinateur dit question de l’impact environnemental. J’exclus complètement le
neuf et je commençais à regarder du côté du reconditionné
([Backmarket](https://www.backmarket.fr/) ou, plus proche, [chez AfB](https://www.afbshop.fr)).
Ce n’est pas parfait, mais il me faut quand même un outil de travail qui ne me
lâche pas du jour au lendemain.

Je me déciderai sans doute cette semaine.
