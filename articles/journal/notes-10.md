---
title: Notes #10
date: 2020-05-11 09:30
blog: false
---

Déjà dixième note. Je perçois désormais un peu mieux à quoi pourront me servir
ces notes dans le futur et j’ai envie d’avoir un peu plus de structure dans la
rédaction de celles-ci. Certains sujets reviennent régulièrement et j’aimerais
en faire un suivi plus rigoureux.

## Flus

### Ce que j’ai fait

J’ai créé le dépôt pour le logiciel qui tournera derrière le service
[flus.fr](https://flus.fr). Il s’appellera [flusio](https://github.com/flusio/flusio)
(pour contenter les personnes qui prononçaient déjà le nom du service comme
ça 😉). J’ai mis sur place tout l’environnement technique : PHP (avec [Minz](https://github.com/flusio/Minz),
un framework maison), PostgreSQL, et un peu de JavaScript avec [Stimulus](https://github.com/stimulusjs/stimulus)
et [Turbolinks](https://github.com/turbolinks/turbolinks). Je souhaitais
garder le tout le plus simple possible, mais il y avait des choses que je
n’avais pas envie de me retrouver à faire plus tard (comme ajouter le
« bundler », j’ai choisi [Parcel](https://parceljs.org/)). Je pensais que
l’ajout des gestionnaires de paquets (Composer et NPM) me simplifierait la vie
future, mais je continue de penser que ça ajoute une couche de complexité
importante et qu’on ferait mieux de limiter les dépendances au strict minimum…
c’est ce que j’essaye de faire. Toutefois le tout me semble plus lourd que ce
que j’espérais initialement ; j’imagine que c’est inévitable pour un projet de
cette envergure. L’important reste le résultat final

J’ai aussi dû ajouter le support de PostgreSQL à mon framework qui ne gérait
que SQLite jusqu’à maintenant. J’ai dû m’y reprendre à deux fois pour obtenir
ce que je voulais. Je sens que ça reste encore un peu fragile comme implémentation,
mais je suis satisfait du résultat ([voir le commit de merge](https://github.com/flusio/Minz/commit/c3a4b9f57c65992b52f365d459823c5d80be3d45)).

J’ai fait d’autres modifications pour être en mesure de gérer un projet plus
gros que ce que j’ai fait jusqu’à maintenant avec Minz.

Enfin, d’un point de vue non-technique (quand même !) on a bossé avec
[Maiwann](https://www.maiwann.net/) qui va me donner un coup de main sur la
conception de l’interface. J’ai dû essayer de lui transmettre ce que je
comptais faire d’un point de vue global mais suffisamment détaillé. C’était un
exercice compliqué mais hautement nécessaire. J’ai pu éclaircir quelques zones
de flou, mais surtout j’ai véritablement mis au propre les idées que j’avais.

### Ce que j’ai appris

J’ai appris à configurer l’exécution des tests via les « GitHub Actions ». Je
sens que c’est bien plus puissant que chez Gitlab ou Travis, mais j’ai
également l’impression de moins comprendre ce que je fais en manipulant des
« boîtes noires » créées par d’autres. Je ne trouve pas non plus la
documentation super, heureusement qu’il existe des tas d’exemples desquels
s’inspirer.

Plus intéressant pour moi, j’ai appris à configurer [Turbolinks](https://github.com/turbolinks/turbolinks).
Je connaissais déjà de mon expérience avec Rails, mais c’était directement
embarqué. Finalement ce n’est pas beaucoup plus compliqué que de faire :

```javascript
import { Application } from 'stimulus';
Turbolinks.start();
```

Derrière, tous les liens sont chargés en JavaScript pour éviter un rechargement
complet de la page. Ça donne une impression de chargement plus rapide en
évitant un effet « page blanche » à chaque nouveau clic.

### Ce que je vais faire

La prochaine étape est de développer les écrans d’inscription. Pas de CSS pour
l’instant, ce sera moche mais fonctionnel. J’ai déjà détaillé le processus par
écrit, je me demande si ce serait intéressant de rendre ce travail public…

J’ai prévu deux semaines de travail. C’est large, mais je veux soigner tous les
détails de cette étape qui est essentielle. Et les détails, c’est ce qui bouffe
tout mon temps.

## Dessin

### Ce que j’ai fait

J’ai fêté le premier mois de dessin quotidien en m’octroyant une journée sans
dessin. C’était une manière de me rappeler qu’il n’y a aucune pression dans
cette activité. Pour autant je prends toujours du plaisir à pratiquer un peu
tous les jours. J’essaye de varier les exercices d’observation, de perspective
et de composition. Je sens que c’est la base dont j’avais besoin pour me sentir
progresser car je peux déjà dessiner beaucoup de choses avec ça. À cela
s’ajoute quelques exercices de mise en valeur (contrastes, hachures, valeurs de
trait) et je commence doucement à apprendre les proportions du corps humain.

Je continue de passer par le site [Dessindigo](https://www.dessindigo.com/). Je
trouve que leurs exercices offrent des défis suffisamment élevés pour sentir la
progression une fois terminés, mais pas trop compliqués de sorte qu’on
n’abandonne pas en cours de route. Ils proposent également des corrections en
vidéo ainsi qu’un forum où les administrateur·ices peuvent corriger nos dessins
(même si j’ai l’impression qu’ils et elles sont un peu débordé·es par moment).

### Ce que j’ai appris

Dessiner des abeilles et des poules \o/. J’ai fait deux petites coccinelles et
une abeille sur des cailloux peints qu’on a ensuite mis dans la cour de
l’immeuble où un mini-potager commence à prendre forme : ma première exposition
publique en quelque sorte ! 😁

J’ai aussi appris plein de trucs sur la perspective (à 1, 2 et 3 points de
fuite). Je me rends compte que toutes mes précédentes tentatives de dessin de
paysage urbain étaient ratées à cause d’un manque de notion dans ce domaine. Ça
change tout une fois qu’on a compris.

### Ce que je vais faire

Je suis en train de me créer doucement un catalogue de photos de référence, je
vais donc le compléter au fil du temps. Je démarre souvent par une recherche
sur [unsplash.com](https://unsplash.com) qui a l’avantage de proposer des
photos esthétiques.

Je me suis donné pour objectif de dessiner une courte histoire sans dialogue,
en quelques cases. Ça va me permettre de pratiquer tout ce que j’ai appris
jusque-là. Si j’en suis suffisamment content, peut-être que je la publierai,
mais je ne promets rien (je suis extrêmement exigeant envers moi-même, ce qui
explique que j’ai abandonné plus d’une fois le dessin…)

## Pain

### Ce que j’ai fait

N’importe quoi, littéralement. J’ai tendance à faire toujours la même recette
parce que je suis toujours dans une phase d’apprentissage de certaines bases et
que je sens qu’il y a des trucs à améliorer. Cependant on va pas se le cacher :
faire du pain, ce n’est pas toujours hyper fun, en particulier quand il faut
pétrir le pain à la main. Par conséquent j’ai eu tendance ces derniers jours à
le faire à l’arrache en ne respectant quasiment pas les temps de lever, ou en
faisant n’importe quoi dans le dosage des ingrédients.

C’est en plus un peu compliqué en ce moment puisque la température augmente,
les temps de levée changent en même temps.

### Ce que j’ai appris

De façon un peu contradictoire avec ce que j’ai expliqué au-dessus, j’ai quand
même appris à mieux respecter le temps de pétrissage (entre 10 et 15 minutes).
Je mets également un peu moins d’eau dans la pâte car je n’avais pas pris en
compte celle qui compose mon levain. Je cherche toutefois encore le bon dosage.

Aussi, je fais toujours une petite boule de 100g de pâte supplémentaire,
que je découpe avant la phase de façonnage du pain. Toutefois je pense que ce
n’est pas une bonne chose parce que le CO2 doit s’échapper et donc le pain doit
moins gonfler. J’ai quand même un doute sur le fait que ça ait un fort impact…

### Ce que je vais faire

À raison de 2 pains par semaine, j’aimerais quand même commencer à varier les
types de pain, ainsi que les formes. Je pense donc changer de façon de faire
une fois sur deux, la deuxième fois étant pour continuer à pratiquer ma manière
« traditionnelle ».

---

En vrac :

- Ma connexion Internet recommence à faire du yoyo, pas pratique pour faire le
  backup de ses données…
- J’ai pété hier mon wiki en voulant faire sa mise à jour, 2h de perdu parce
  que tout à coup la connexion à la base de données ne fonctionnait plus… je
  déteste devoir utiliser des technos que je ne maîtrise pas.
- Mon rythme de travail limité à 6h par jour est compliqué à tenir quand je
  prends du plaisir à ce que je fais. J’ai dû tourner certains jours autour des
  8h, et même quelques heures hier parce que je n’arrivais pas à me défaire
  d’un truc que j’avais en tête.
- Je suis content qu’ils prévoient encore de la pluie pour quelques jours, même
  si ça pourrit un peu le déconfinement, c’est sans doute pas une mauvaise
  chose 😉
