Date: 2013-10-05 11:16
Title: DesperadOS, le système qui fait bang!
Slug: desperados-le-systeme-qui-fait-bang
Tags: desperados, c
Summary: DesperadOS c'était le projet le plus enthousiasmant réalisé durant mes études. Développer un système d'exploitation (quasiment) *from scratch* a été très formateur et j'en garde un excellent souvenir.

Aujourd'hui le monde de l'informatique se sépare entre Windows et Mac. Parfois il arrive qu'un barbu sorte de sa salle de serveurs pour râler "*Gnu/Linux, pas Linux*", mais ça reste anecdotique. Si je prends le clavier ce jour, c'est pour vous parler d'un nouveau système d'exploitation qui va très certainement révolutionner votre vie.

## Fonctionnalités

Un système innovant se doit de proposer des technologies à la pointe. DesperadOS ne se contente pas d'être innovant, il est révolutionnaire !

  * Système multi-processus avec ordonnancement de type [Round-Robin](https://fr.wikipedia.org/wiki/Round-robin_%28informatique%29)
  * Gestion bas-niveau des sémaphores
  * Gestion du clavier, de l'écran ET du temps
  * Écran de veille en mode VGA
  * Système de fichiers avec arborescence en mémoire
  * Shell incluant pas moins de **21 commandes**

L'authentification par empreinte biométrique est en cours d'intégration, la révolution est en marche.

## La vidéo qui va bien

Vous pouvez bien entendu douter de ma parole et remettre en cause le bien-fondé de mon enthousiasme. C'est sans compter que je ne suis pas venu les mains dans les poches. Cette vidéo saura très certainement vous faire changer d'avis.

<iframe src="http://www.youtube.com/embed/R-NDlywRZQY" allowfullscreen="" width="640" frameborder="0" height="360"></iframe>

## Genèse du projet

En juin dernier, alors que le soleil et la pluie se faisaient la cour, un certain nombre d'individus savourait les joies de ne pas pouvoir profiter de leur temps libre. Une mission les réunissait : développer le système d'exploitation qui allait maximiser leur note de projet.

Si je vous en parle, c'est parce que j'ai moi-même participé à ce grand beau projet. J'étais là quand les profs ont présenté le sujet. J'étais là aux premières lignes de code. J'étais encore là lors du premier bug. J'étais aussi là les midis, au moment de faire le grand choix "pâtes ou sandwichs". Oui, je peux affirmer que j'étais là car j'ai moi-même subi la première semaine sans clim' en haut de ce bâtiment exposé au soleil en plein milieu de l'après-midi.

Bien entendu, je n'étais pas seul : [Paul](http://unpetitfrancaisaprague.com/) et [Micko](http://fr.linkedin.com/in/mikolajpawlikowski) complétaient notre groupe.

## Le concept

Pour ceux qui ne l'ont pas encore compris, bien entendu DesperadOS n'est en rien révolutionnaire. Encore que sa façon de planter durant la démo... mais Microsoft nous a devancé. Deux fois.

Durant deux semaines et demi en juin dernier, par groupe de trois, nous avons donc eu à développer un système d'exploitation. À savoir que nous partions d'une base de code fournie mais qui ne faisait basiquement... rien (ce qui n'est qu'à moitié faux). À partir de ce rien du tout, il nous a donc fallu gérer l'affichage à l'écran, le temps, l'ordonnancement des processus, le clavier et j'en passe. Autant dire que c'était bien fun ☺

## La gestion du projet

Nous étions seulement trois pour ce projet donc la gestion fut assez simple. Nous avons utilisé [Trello](https://trello.com/) pour tout ce qui fut gestion des tâches et je dois avouer que c'était plutôt efficace. Selon moi, ça reste néanmoins un outil à utiliser en groupe et je n'ai jamais réussi à l'utiliser (trouvé l'utilité ?) dans un projet perso.

Git fut bien entendu aussi de la partie pour la gestion du code et comme nous avions tous un minimum d'expérience avec, ça n'a pas posé de soucis particulier (j'aime Git <3)

## Rapide retour d'expérience

Les deux semaines et demi passées sur ce projet ont vraiment été géniales. Nous avons *enfin* pu comprendre comment fonctionne un système d'exploitation au plus bas niveau (les joies de l'assembleur :D), la gestion des processus et des ressources. C'est vraiment un projet à ne pas rater et où l'on peut prendre son pied si on s'applique.

Bien sûr il y a eu quelques (nombreuses) difficultés qui nous ont fait désespérer de finir le projet... mais les surmonter ne fut que plus enthousiasmant. Notons tout de même que notre code ne fut pas parfait et nous sommes passés à côté d'un ou deux éléments importants. Mais je pense que le plus important reste d'avoir compris dans les grandes lignes le fonctionnement d'un système d'exploitation.

Depuis j'ai rapidement essayé de me plonger dans le code de Linux. Force est de constater que le code est... dense. J'ai abandonné. Il y a eu quelques articles d'introduction sur [LinuxFR](http://linuxfr.org/) à l'époque, mais impossible de les retrouver.

Sans doute plus intéressant et plus facile d'accès, [Manux](http://www.manux.info/fr/) (non, il ne s'agit pas d'un mauvais [jeu de mot](http://doug.letough.free.fr/images/manux.jpg)). Système d'exploitation purement français (les fichiers, le code, les commentaires : tout est en Français) et issu... du projet système de l'Ensimag, il y a déjà quelques années de ça ! Le développeur avait fait [une annonce lors de la sortie de la version 0.0.1](http://linuxfr.org/users/ecolbus/journaux/annonce-manux-0-0-1) sur LinuxFR.

## Et la suite

J'aurais voulu pouvoir partager le code qui devrait d'ailleurs être en grande partie (voire totalement) sous licence libre, néanmoins je n'ai pas le droit de fournir les solutions aux futurs étudiants. Et dans le cas où ils passeraient par ici, je ne peux évidemment pas vous mettre le code à disposition. Dans le cas où il m'arriverait **par hasard** de vouloir reprendre en totalité le projet pour obtenir quelque chose de plus abouti (hum...) et différent du projet de base, je me ferai une joie de partager le code... Mais je ne me fais pas trop d'illusions ;)

Si vous voulez plus d'infos sur ce projet, [notre page de (ensi)wiki](https://ensiwiki.ensimag.fr/index.php/Projet_syst%C3%A8me_PC_:_2013_-_Mikolaj_PAWLIKOWSKI,_Marien_FRESSINAUD_et_Paul_AMAR) décrit un peu mieux l'OS. Vous pouvez aussi voir [le travail des autres groupes](https://ensiwiki.ensimag.fr/index.php/Projet_syst%C3%A8me_:_r%C3%A9sultats) (il y en a des vraiment [impressionants](https://ensiwiki.ensimag.fr/index.php/Projet_syst%C3%A8me_PC_:_2010_-_Damien_Dejean_et_Gaetan_Morin), mais ils avaient plus de temps :p). Et enfin, la page qui présente [le sujet du projet](https://ensiwiki.ensimag.fr/index.php/Projet_syst%C3%A8me). C'était en tout cas le projet le plus intéressant que l'on ne m'ait jamais proposé et ce serait avec joie de continuer encore un peu. Je me demande juste pourquoi j'ai mis autant de temps à écrire cet article.
