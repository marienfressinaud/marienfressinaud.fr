---
title: Sur l’objectif du GDPR.txt
date: 2022-03-10 12:10
---

J’ai publié [un article sur le Framablog](https://framablog.org/2022/03/08/faciliter-la-conformite-rgpd-le-fichier-gdpr-txt/) pour présenter le fichier GDPR.txt (présenté [ici](gdpr-txt.html) il y a quelques jours). Les premiers retours n’ont pas tardé, ainsi qu’une incompréhension majeure : celle de l’objectif même de ce fichier… c’est embêtant ! Cette incompréhension s’exprime sous la forme de deux suggestions :

- que le fichier soit placé sous une <abbr>URL</abbr> de type `/.well-known/` ;
- que le fichier utilise un format structuré existant (ex. YAML).

<i lang="la">In fine</i>, il est demandé à ce que ce fichier soit facilement accessible à un navigateur — ou autre logiciel — afin d’exposer les informations de conformité <abbr>RGPD</abbr> des sites. Je le redis ici : ce n’est pas la proposition que j’ai faite.

**Ma proposition est de mutualiser le travail de recherche des informations en laissant aux développeurs et développeuses le soin de déclarer les données et finalités de leurs logiciels.** C’est un fichier écrit par des humains, à destination d’autres humains ; de la documentation en somme.

Cela étant dit, je trouve qu’un fichier standardisé sous `/.well-known/` serait une excellente idée : c’est celle que j’avais en tête en commençant à travailler sur le <abbr>RGPD</abbr> à Framasoft d’ailleurs 😊 Pourquoi ma proposition est différente alors ?

- dans l’immédiat ça ne nous aide pas à Framasoft et on a décidé de viser la conformité en priorité ;
- je ne veux pas proposer un tel format sans l’avis d’un ou une juriste spécialisée ;
- je n’ai pas l’énergie de m’occuper de ça (pour l’instant).

**Ça viendra donc peut-être un jour, surtout si d’autres personnes s’emparent du sujet (et je vous y invite fortement !)** Je veux d’ailleurs bien servir de relais entre ces personnes.

---

Pour creuser l’incompréhension, j’ai identifié trois raisons :

1. les personnes qui se sont intéressées au fichier sont des personnes qui cherchent elles-mêmes à se conformer au <abbr>RGPD</abbr>, pas nécessairement des développeurs et développeuses (qui sont ma cible principale) ;
2. le nom même rappelle le fichier « robots.txt » qui est un fichier à destination de machines et non d’humains ;
3. j’ai proposé un format pseudo-structuré, alors qu’un format libre aurait été largement suffisant (puisqu’on est plus sur de la documentation qu’autre chose).

Ma proposition serait donc de virer toute la partie structuration du fichier et de concentrer mes explications sur comment bien remplir le fichier. Je pense également laisser libre le format du fichier (`GDPR.md` serait tout aussi bien par exemple).

**→ Je suis [preneur de vos suggestions et remarques sur le sujet.](https://framagit.org/marienfressinaud/gdpr-txt/-/issues)**