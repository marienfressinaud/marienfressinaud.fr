Date: 2013-05-05 13:03
Title: Welcome to FreshRSS 0.3!
Slug: welcome-to-freshrss-0-3
Tags: php, freshrss
Summary: FreshRSS est le premier projet conséquent que j’ai porté. Initié en novembre 2012, la version 1.0 est sortie début 2015 seulement. J’annonçais peu après que je donnais les clés à la communauté pour reprendre le code [disponible sur GitHub](https://github.com/FreshRSS/FreshRSS). J’ai toutefois aujourd’hui toujours des idées pour l’améliorer.

Non je n’écrirai pas cet article en anglais comme peux le laisser penser ce titre, mais il s’agit d’introduire la plus grosse nouveauté de cette nouvelle version toute fraîche de mon agrégateur de flux RSS : l’internationalisation de l’application ! L’anglais et le français sont donc désormais pris en charge intégralement ; FreshRSS va pouvoir partir à la conquête du nouveau continent. N’étant pas forcément bon en orthographe en anglais, n’hésitez pas à me remonter les erreurs de traduction ☺

## Nouveautés

Mais s’il s’agit de la plus grosse nouveauté, il ne faut pas oublier toutes les autres qui ne sont pas non plus en reste. Au programme de cette nouvelle version donc :

- Une page dédiée qui sert de site officiel
- Internationalisation
- Création d’un logo **temporaire**. Je trouve celui que j’ai fait à l’arrache de plus en plus laid mais le problème est que je n’ai aucun talent de graphiste ☹ Je prendrai le temps d’en faire un mieux pour la version 1.0 ou je ferai appel à quelqu’un peut-être...
- Meilleure gestion CSS3 pour les navigateurs ne supportant pas les dégradés ou les transitions de façon "officielle" (utilisation des préfixes propriétaires bien que j’en ai horreur)
- Possibilité de s’abonner à des flux derrière une authentification HTTP (c’était déjà le cas, mais pas réellement de façon officielle, et les identifiants apparaissaient totalement en clair)
- Mise en cache des favicons (le site [getFavicon](http://g.etfv.co/) va être soulagé)
- Affichage des vidéos incluses dans les articles (SimplePie les enlevait par défaut)
- Une bien meilleure gestion de la recherche et du filtrage par tags ! Ça m’a demandé beaucoup de temps et une bonne prise de tête pour en arriver là mais j’en suis assez content. Problème : il se peut que les performances soient fortement dégradées. Si vous vous rendez compte que votre serveur ne tient pas la charge, faites-le moi savoir, j’essayerai de voir ce que je peux faire. Ceci dit, sur mon instance qui stocke pour le moment 1600 articles ça passe très bien. Petit plus : vous pouvez accéder au flux RSS d’une recherche ou d’un filtre. Une fois votre recherche lancée, il suffit de rajouter le paramètre "&output=rss" dans l’url ou de cliquer simplement sur le bouton à côté de "Gestion des abonnements"
- J’ai créé un "vrai" script CRON de façon à pouvoir mettre tous les flux à jour d’un coup sans que le serveur vous rejette avec un timeout.
- Et bien sûr, divers corrections de bugs avec une revue du code pour qu’il soit plus clair

## Mise à jour

À priori la mise à jour se fait très simplement : il vous suffit de télécharger la nouvelle version et d’écraser les anciens fichiers avec les nouveaux. Pensez à supprimer le fichier `./public/install.php` qui ne vous sert à rien si vous faites une mise à jour, ou alors on vous redemandera les informations que vous aviez rentré à la première installation (ça marchera quand même, mais ça ne sert à rien ;)). Pas de mise à jour de la base de données cette fois-ci, donc si vous tourniez correctement avec la version 0.2, c’est tout bon.

## Et la suite ?

La version 0.4 est en approche, mais il se pourrait que ce soit aussi la version 1.0, tout dépend des idées qui me viendront pour la suite. Les nouveautés à prévoir sont les suivantes :

- Changer un flux de catégorie par drag and drop
- Version mobile : passer à l’article suivant/précédent par effet de slide (glissement du doigt vers la gauche pour aller à l’article suivant, vers la droite pour passer au précédent)
- Ajout de vues "Lecture" et "Globale" : la vue "Lecture" sera dépourvue d’éléments "perturbateurs", la vue "Globale" offrira une vue permettant de voir en un coup d’œil quels sites ont publié depuis votre dernière visite
- Optimisation de la table en base de données : c’est un petit truc que j’utilise personnellement pour réduire la place utilisée en base de données par les articles (voir [https://dev.mysql.com/doc/refman/5.5/en/optimize-table.html](https://dev.mysql.com/doc/refman/5.5/en/optimize-table.html))
- Gérer les soucis de flux : permettra de voir quels sont les flux qui n’ont pas réussi à se mettre à jour en le mettant en rouge par exemple
- Possibilité de filtrer les tags en cliquant dessus (via "Tags associés" dans un article)

Si vous souhaitez voir apparaître d’autres fonctionnalités, n’hésitez pas à me les soumettre, je suis preneur ! Mais de préférence faites-le d’ici dimanche prochain : après, les demandes seront prises en compte pour la version 0.5.
