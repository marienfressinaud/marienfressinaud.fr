Date: 2012-10-28 14:15
Title: FreshRSS : un agrégateur simple et léger
Slug: freshrss-un-agregateur-simple-et-leger
Tags: php, freshrss, featured
Summary: FreshRSS est le premier projet conséquent que j’ai porté. Initié en novembre 2012, la version 1.0 est sortie début 2015 seulement. J’annonçais peu après que je donnais les clés à la communauté pour reprendre le code [disponible sur GitHub](https://github.com/FreshRSS/FreshRSS). J’ai toutefois aujourd’hui toujours des idées pour l’améliorer.

**Note importante du 16 mai 2013** : le projet a **beaucoup** évolué depuis cet article. Comme je vois toujours beaucoup de liens pointant ici, je tiens à vous rediriger vers des liens plus actuels.

- [Le site officiel](http://freshrss.org)
- [Le dépôt Github](https://github.com/FreshRSS/FreshRSS)
- [La démo](http://demo.freshrss.org)

**L’article original du 28 octobre 2012**

Quand on parle d’agrégateurs de flux RSS, on a souvent en tête [RSSLounge](http://rsslounge.aditu.de/) et [TinyTinyRSS](http://tt-rss.org), les deux poids lourds du secteur (j’exclue volontairement les solutions qu’on ne peut pas auto-héberger, vous l’avais compris). Pour certains d’entre vous, il y aura aussi [Leed](http://blog.idleman.fr/?p=1233), le petit dernier développé par Idleman. Mais aujourd’hui, je vous offre ma propre solution faite maison : FreshRSS (licence AGPL3).

**Tout d’abord, expliquons le pourquoi.** Ceux qui ont déjà manipulé RSSLounge savent que ce n’est pas franchement ce qu’il y a de plus léger. Je n’ai jamais essayé TinyTinyRSS mais tout le monde s’accorde pour dire qu’il est encore plus lourd que le précédent. J’utilisais personnellement RSSLounge tant bien que mal, en pestant plus qu’autre chose lorsque je voulais ajouter un flux RSS. La surcouche javascript rendant les choses particulièrement indigeste.
Puis vint Leed, promesse d’un agrégateur plus clair, plus léger, plus simple. Je pense que cette promesse a été tenue par Idleman. Mais voilà, je suis un éternel insatisfait et il ne m’a pas convaincu, notamment à cause de l’espace pris sur l’écran mal optimisé et le manque de raccourcis.

…

Bon d’accord, je me cherchais juste une excuse pour me développer mon propre agrégateur ! C’était aussi une occasion de prouver que mon framework PHP pouvait s’adapter à plusieurs usages de façon très modulaire.

**Bref, pour éviter de tourner autour du pot**, voilà ce que peut vous apporter FreshRSS :

- Un système léger, non alourdi par une tonne de javascript (beaucoup de choses se font en PHP). Il y a du javascript certes, mais il sert à faciliter la navigation et tout peut fonctionner sans.
- Une interface que j’espère optimisée pour la lecture de vos flux
- Des raccourcis pour naviguer et pour marquer les articles comme lu / non lus, favoris / non favoris. **Ces raccourcis sont paramétrables**.
- L’importation / exportation OPML (attention, j’ai eu des soucis d’importation avec le fichier exporté par RSSLounge)
- La possibilité de lancer une tâche CRON sur l’url mettant à jour les flux
- Système d’authentification basé sur [Persona](https://persona.org/) de Mozilla (bon, c’est encore assez bancal je trouve et ça utilise du javascript...) Personnellement je bloque l’accés à mon flux RSS par une authentification HTTP.
- Catégorisation des flux (comme la plupart des agrégateurs)
- Un affichage correct sur écran de smartphones (mais pas optimisé)
- Et surtout, pas trop de trucs "bling bling" ☺

**Le mieux reste de [tester FreshRSS](http://demo.freshrss.org).** J’ai bien évidemment bloqué l’accés à la configuration ici pour éviter que vous ne changiez tout (ce sont mes propres flux qui s’affichent ;)). Pour les touches de raccourcis, vous pouvez utiliser :

- `page down` et `page up` pour naviguer entre les articles
- `gauche` et `droit` pour naviguer entre les pages
- `espace` pour ouvrir l’article sélectionné dans un nouvel onglet

Petite précision avant de terminer : FreshRSS a été développé dans un but strictement personnel, sans aucune prétention et s’adapte à mes propres besoins. Je n’ai pas immédiatement cherché à faire en sorte qu’il s’adapte à d’autres usages que le mien (l’ajout de la connexion via Persona est la seule exception car je ne l’utilise pas). Néanmoins, si vous souhaitez voir apparaître de nouvelles fonctionnalités vous pouvez me les soumettre... mais ce n’est pas dit que j’y réponde dans l’immédiat :p

Oh, et puis ce n’est qu’une version alpha pour le moment ;)
