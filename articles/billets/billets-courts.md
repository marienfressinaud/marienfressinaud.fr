---
title: Billets courts
date: 2022-01-24 12:05
---

J’ai pris la bonne résolution d’écrire plus souvent sur ce blog. Comme je suis plutôt mal parti en ce début d’année, j’ai pris la seconde bonne résolution de publier un billet — court — par jour[^1], histoire de ne pas faire dans la demie-mesure. Pour me faciliter la tâche, j’en ai préparé quelques-uns à l’avance. J’emprunte le terme de « billet » à [Nicolas](https://nicolas-hoizey.com/billets/2022/01/12/) ; j’aime bien le terme.

[^1]: Remarquez que je ne parle pas de durée :)

Comme je risque de parler de Flus, il y a des chances que certains billets soient publiés [dans son carnet](https://flus.fr/carnet) plutôt qu’ici.

Je vois de plus en plus de blogs personnels apparaître dans mes lectures. Ça me fait plaisir de voir les usages revenir doucement vers ce format, alors autant y participer !
