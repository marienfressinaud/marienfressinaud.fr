Date: 2013-02-28 19:20
Title: Mise à jour du site - la v5 est là \o/
Slug: mise-a-jour-du-site-la-v5-est-la-o
Tags: màj
Summary: Mon site existe depuis 2010. Lors de la migration d’avril 2016, j’ai fait le grand ménage dans les articles. Je trouvais toutefois intéressant de conserver les articles liés à la vie du site pour voir comment celui-ci a grandi.

J’ai mis mon site à jour en fin de semaine dernière. Cette dernière version est très inspirée par [Shaarli de Sebsauvage](http://sebsauvage.net/links/), tout en allant un poil plus loin. Le principe de Shaarli est de partager des liens qui nous paraissent pertinent et de faire un rapide commentaire dessus, le tout, sans se prendre la tête. Mon problème étant que je n’ai pas que des liens à partager, et même si je n’avais que ça, ils se retrouvent assez dispersés.

En gros il m’arrive de publier des liens, mais aussi des articles de blog ainsi que ce que j’appelle des statuts (oui oui, à la façon Facebook). Les liens que je partage passent soit par ma version maison de Shaarli (Links), soit par [mon lecteur (maison) de flux RSS (FreshRSS)](https://github.com/FreshRSS/FreshRSS). De plus, je pourrais envisager d’autres outils de partage.

Je me retrouve donc pour le moment avec 3 outils de partage différents. Mon soucis était de tout publier automatiquement sur le site, dans un flux uni. C’est là qu’intervient mon outil Uniflux. Celui-ci permet de s’abonner à différents flux de type Uniflux et regroupe tout ce qu’il trouve dans un flux unique. Je n’ai plus qu’à afficher côté client (le site) les articles, les liens et les statuts.

Ça c’était pour l’introduction... mais maintenant plusieurs questions peuvent se poser et je vais essayer d’y répondre.

## Un schéma, un schéma !

Oui, de suite !

**Note** : il n’y a aujourd’hui plus de schéma, mais ce n’est pas grave, Uniflux n’existe plus :)

## Pourquoi avoir développé un Shaarli maison ?

Bien que je pense que Sebsauvage ait fait du bon travail, tout ne me satisfait pas dans Shaarli. Tout d’abord, quasiment tout le code se trouve dans le fichier principal : il en devient presque in-maintenable et pas du tout modulaire. J’ai eu à plusieurs reprises besoin d’un morceau de code venant de Shaarli (génération du nuage de tags, récupération des thumbnails externes, etc.) et bien vite on se retrouve à devoir récupérer d’innombrables fonctions pour que ça fonctionne. C’est dommage parce que ça pourrait être super utile en étant mieux découpé ☹

Autre argument, il y a trop de fonctionnalités que je n’utilise pas du tout et que je ne juge pas pertinentes pour mon usage... d’autant plus que je n’avais besoin de mon côté que de la partie "serveur".

Enfin... j’aime bien tout refaire de mon côté pour manipuler un peu et apprendre par moi-même, et ça, c’est la meilleure des excuses ☺

## Pourquoi avoir inventé un nouveau format (Uniflux) alors que RSS pourrait très bien faire l’affaire ?

Oui parce qu’au final Uniflux n’est qu’un agrégateur d’articles et de liens... Il y a tout de même quelques soucis avec RSS (bien que j’en sois un fervent défenseur !). Déjà, le format est lourd (le XML a du bon, mais pas sa verbosité). Uniflux se contente d’une API en JSON que je trouve certes moins lisible, mais plus légère et tout aussi efficace. De plus ça me permet de faire évoluer le format comme je l’entends.

Si j’ai le courage de continuer, Uniflux gérera aussi un jour le flux de commentaires associé à un post (du coup mon site ne gère plus les commentaires, mais le ratio spam / commentaires pertinents était trop élevé pour que je passe du temps là-dessus)

## Quel est l’intérêt ?

Mon soucis comme je l’ai dit est que je possède différents outils pour communiquer (le blog, Links et FreshRSS. Vu que j’ai incorporé à chacun l’export au format Uniflux, il me suffit d’ajouter l’url de l’API d’export et mon flux est automatiquement alimenté. Si jamais un jour je décide de créer un outil de gestion de fichiers, je n’aurai qu’à ajouter un export au format Uniflux pour partager des fichiers que je souhaite rendre public. Automatiquement le partage sera disponible sur mon site. À vrai dire, il me suffit d’ajouter un export au format Uniflux à n’importe quelle application web, et mon outil pourra s’y connecter naturellement et alimentera mon site.

C’est un peu ma bataille à moi contre la dispersion de nos informations. En effet, je suis parti du constat que je partageais des choses sur Facebook, Google+, Diaspora*, etc. et que j’aurais bien voulu que tout se retrouve dans un flux unique. Alors j’ai décidé d’arrêter d’utiliser ces outils et de me contenter de mes propres outils. Et ça me convient parfaitement. C’est ce que je cherchais et ça répond à mes besoins. C’est simple, efficace et je l’ai fait avec mes petites mains ☺

## Mais... c’est utilisable ?

Franchement ? Non. Enfin si, mais pour le moment, que par moi je pense. Ça manque de documentation, ça manque d’une idée claire sur ce que je veux gérer, ça manque d’une bonne utilisation et ça manque surtout d’énormément de recul. Je publierai le code de tout ça un de ces jours à l’état de proof of concept mais je compte remanier un peu le bouzin afin d’avoir un truc exportable. Si finalement j’arrive à clarifier mes idées, j’intégrerai tout ça dans Minz (mon framework PHP) afin de pouvoir créer des applications connectées en quelques lignes. Maiiis... ça attendra encore un peu, j’ai des partiels avant :p

## Conclusion

Voilà donc un rapide état des lieux du boulot fourni depuis plusieurs mois (ça fait quand même pas mal d’applications écrites). J’aime autant préciser (je le fais à la fin, mais j’aurais peut-être dû le faire au début...) : je n’assure aucun suivi sur aucune des applications que j’ai développées, mise à part Minz. Tout ce que j’ai fait a été développé dans un but personnel pour répondre à mes besoins. Néanmoins j’envisage de refondre un peu mon lecteur de flux RSS et d’assurer de la maintenance dessus.
